use futures_util::{stream::repeat, StreamExt, TryStreamExt};
use memcached_client::{Client, Entry, Expiry, Record, Request, Response, Token, Transaction};
use memcached_test::{Memcached, MEMCACHED_VERSION};
use serde::{Deserialize, Serialize};
use std::convert::TryInto;
use std::path::Path;
use testcontainers::clients::Cli;
use tower::{Service, ServiceExt};

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct File {
    pub name: String,
    pub group: Option<String>,
}

#[tokio::test]
async fn smoke_test() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let path = Path::new("/lorem/ipsum");
    let file = File {
        name: "dolor".into(),
        group: Some("sit".into()),
    };
    let token = client
        .add(path, &file, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    let file = File {
        name: "dolor".into(),
        group: None,
    };
    client
        .replace(token, &file, Expiry::default())
        .await
        .unwrap()
        .unwrap()
        .unwrap();

    let record: Record<File> = client.get(path).await.unwrap().unwrap().try_into().unwrap();
    assert_eq!(record.value.name, "dolor");

    client.delete(record.token).await.unwrap().unwrap();

    client.flush(Expiry::default()).await.unwrap();
}

#[tokio::test]
async fn can_fetch_version() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    assert_eq!(client.version().await.unwrap(), MEMCACHED_VERSION);

    let resp = client
        .ready()
        .await
        .unwrap()
        .call(Request::version())
        .await
        .unwrap();

    assert!(matches!(resp, Response::Version(v) if v == MEMCACHED_VERSION));

    let n = 100;
    let requests = repeat(Request::version()).take(n);
    let mut resp = client.ready().await.unwrap().call_all(requests).unordered();
    let mut i = 0;
    while let Some(resp) = resp.try_next().await.unwrap() {
        i += 1;
        assert!(matches!(resp, Response::Version(v) if v == MEMCACHED_VERSION));
    }
    assert_eq!(i, n);
}

#[tokio::test]
async fn can_flush() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";
    let dolor = "dolor";
    let sit = "sit";

    client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    client
        .ready()
        .await
        .unwrap()
        .call(Request::flush(Expiry::now()))
        .await
        .unwrap();

    let resp = client.get(lorem).await.unwrap();
    assert!(resp.is_none());

    client
        .add(dolor, sit, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    client.flush(Expiry::now()).await.unwrap();

    let resp = client.get(dolor).await.unwrap();
    assert!(resp.is_none());
}

#[tokio::test]
async fn can_add_item() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";

    let resp = client
        .ready()
        .await
        .unwrap()
        .call(Request::add(lorem, ipsum, Expiry::default()).unwrap())
        .await
        .unwrap();
    assert!(matches!(resp, Response::Add(Transaction::Ok(_))));
}

#[tokio::test]
async fn second_add_for_same_key_fails() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";
    let dolor = "dolor";

    let old_token = client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    client
        .add(lorem, dolor, Expiry::default())
        .await
        .unwrap()
        .unwrap_err();

    let Entry { token, value } = client.get(lorem).await.unwrap().unwrap();
    assert_eq!(old_token, token);
    let value: String = value.get().unwrap();
    assert_eq!(value, ipsum);
}

#[tokio::test]
async fn can_get_item() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";

    client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    let resp = client
        .ready()
        .await
        .unwrap()
        .call(Request::get(lorem).unwrap())
        .await
        .unwrap();
    assert!(matches!(resp, Response::Get(Some(_))));
    let value: String = match resp {
        Response::Get(Some(Entry { value, .. })) => value.get().unwrap(),
        _ => panic!(),
    };
    assert_eq!(value, ipsum);
}

#[tokio::test]
async fn get_returns_none_when_key_does_not_found() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let resp = client.get("lorem").await.unwrap();
    assert!(resp.is_none());
}

#[tokio::test]
async fn can_replace_item() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";
    let dolor = "dolor";

    let token = client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    let resp = client
        .ready()
        .await
        .unwrap()
        .call(Request::replace(token, dolor, Expiry::default()).unwrap())
        .await
        .unwrap();
    assert!(matches!(resp, Response::Replace(Some(Transaction::Ok(_)))));

    let value: String = client
        .get(lorem)
        .await
        .unwrap()
        .unwrap()
        .value
        .get()
        .unwrap();
    assert_eq!(value, dolor);
}

#[tokio::test]
async fn can_delete_item_with_transaction() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";

    let token = client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    let resp = client
        .ready()
        .await
        .unwrap()
        .call(Request::delete(token))
        .await
        .unwrap();
    assert!(matches!(resp, Response::Delete(Ok(()))));

    let resp = client.get(lorem).await.unwrap();
    assert!(resp.is_none());
}

#[tokio::test]
async fn can_force_delete_item() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";

    client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    client
        .delete(Token::force(lorem).unwrap())
        .await
        .unwrap()
        .unwrap();

    let resp = client.get(lorem).await.unwrap();
    assert!(resp.is_none());

    client
        .delete(Token::force(lorem).unwrap())
        .await
        .unwrap()
        .unwrap();
}

#[tokio::test]
async fn can_force_delete_when_key_not_found() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    client
        .delete(Token::force("lorem").unwrap())
        .await
        .unwrap()
        .unwrap();
}

#[tokio::test]
async fn can_detect_transaction_failure_on_delete() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";
    let dolor = "dolor";

    let token = client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    client
        .set(Token::force(lorem).unwrap(), dolor, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    client.delete(token).await.unwrap().unwrap_err();
}

#[tokio::test]
async fn can_transaction_ok_on_delete_when_key_not_found() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";

    let token = client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    client
        .delete(Token::force(lorem).unwrap())
        .await
        .unwrap()
        .unwrap();

    client.delete(token).await.unwrap().unwrap();
}

#[tokio::test]
async fn replace_returns_none_if_key_not_found() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";
    let dolor = "dolor";
    let sit = "sit";

    let resp = client
        .replace(Token::force(lorem).unwrap(), ipsum, Expiry::default())
        .await
        .unwrap();
    assert!(resp.is_none());

    let token = client
        .add(lorem, dolor, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    client
        .delete(Token::force(lorem).unwrap())
        .await
        .unwrap()
        .unwrap();

    let resp = client.replace(token, sit, Expiry::default()).await.unwrap();
    assert!(resp.is_none());
}

#[tokio::test]
async fn can_detect_transaction_failure_on_replace() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";
    let dolor = "dolor";
    let sit = "sit";

    let token = client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    let old_token = client
        .replace(Token::force(lorem).unwrap(), dolor, Expiry::default())
        .await
        .unwrap()
        .unwrap()
        .unwrap();

    client
        .replace(token, sit, Expiry::default())
        .await
        .unwrap()
        .unwrap()
        .unwrap_err();

    let Entry { token, value } = client.get(lorem).await.unwrap().unwrap();
    assert_eq!(old_token, token);
    let value: String = value.get().unwrap();
    assert_eq!(value, dolor);
}

#[tokio::test]
async fn can_force_replace_item() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";
    let dolor = "dolor";

    client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    let old_token = client
        .replace(Token::force(lorem).unwrap(), dolor, Expiry::default())
        .await
        .unwrap()
        .unwrap()
        .unwrap();

    let Entry { token, value } = client.get(lorem).await.unwrap().unwrap();
    assert_eq!(old_token, token);
    let value: String = value.get().unwrap();
    assert_eq!(value, dolor);
}

#[tokio::test]
async fn force_replace_item_fails_when_key_not_found() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let resp = client
        .replace(Token::force("lorem").unwrap(), "ipsum", Expiry::default())
        .await
        .unwrap();
    assert!(resp.is_none());
}

#[tokio::test]
async fn can_force_set_item_when_key_not_found() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";

    client
        .ready()
        .await
        .unwrap()
        .call(Request::set(Token::force(lorem).unwrap(), ipsum, Expiry::default()).unwrap())
        .await
        .unwrap();

    let value = client.get(lorem).await.unwrap().unwrap().value;
    let value: String = value.get().unwrap();
    assert_eq!(value, ipsum);
}

#[tokio::test]
async fn can_force_set_item_when_key_exist() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";
    let dolor = "dolor";

    client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    let old_token = client
        .set(Token::force(lorem).unwrap(), dolor, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    let Entry { token, value } = client.get(lorem).await.unwrap().unwrap();
    assert_eq!(old_token, token);
    let value: String = value.get().unwrap();
    assert_eq!(value, dolor);
}

#[tokio::test]
async fn can_cas_set_item() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";
    let dolor = "dolor";

    let token = client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    let old_token = client
        .set(token, dolor, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    let Entry { token, value } = client.get(lorem).await.unwrap().unwrap();
    assert_eq!(old_token, token);
    let value: String = value.get().unwrap();
    assert_eq!(value, dolor);
}

#[tokio::test]
async fn cas_set_fails_when_no_key_found() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";
    let dolor = "dolor";

    let token = client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    client
        .delete(Token::force(lorem).unwrap())
        .await
        .unwrap()
        .unwrap();

    client
        .set(token, dolor, Expiry::default())
        .await
        .unwrap_err();

    let resp = client.get(lorem).await.unwrap();
    assert!(resp.is_none());
}

#[tokio::test]
async fn cas_set_fails_when_key_exist() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    let lorem = "lorem";
    let ipsum = "ipsum";
    let dolor = "dolor";
    let sit = "sit";

    let token = client
        .add(lorem, ipsum, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    let old_token = client
        .set(Token::force(lorem).unwrap(), dolor, Expiry::default())
        .await
        .unwrap()
        .unwrap();

    client
        .set(token, sit, Expiry::default())
        .await
        .unwrap()
        .unwrap_err();

    let Entry { token, value } = client.get(lorem).await.unwrap().unwrap();
    assert_eq!(old_token, token);
    let value: String = value.get().unwrap();
    assert_eq!(value, dolor);
}

#[tokio::test]
async fn cas_quit() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut client = Client::builder().build(memcached.addr());

    client
        .ready()
        .await
        .unwrap()
        .call(Request::quit())
        .await
        .unwrap();

    let client = Client::builder().build(memcached.addr());
    client.quit().await.unwrap();
}
