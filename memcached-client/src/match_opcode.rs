use crate::{BinaryRequest, BinaryResponse};
use futures_util::{future::BoxFuture, FutureExt, TryFutureExt};
use std::{
    io,
    task::{Context, Poll},
};
use tower::{BoxError, Layer, Service};

pub(crate) struct MatchOpcodeLayer;

impl<S> Layer<S> for MatchOpcodeLayer
where
    S: Service<BinaryRequest, Response = BinaryResponse>,
    S::Error: Into<BoxError>,
{
    type Service = MatchOpaqueService<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Self::Service { inner }
    }
}

pub(crate) struct MatchOpaqueService<S> {
    inner: S,
}

impl<S> Service<BinaryRequest> for MatchOpaqueService<S>
where
    S: Service<BinaryRequest, Response = BinaryResponse>,
    S::Error: Into<BoxError> + 'static,
    S::Future: Sized + Send + 'static,
{
    type Response = BinaryResponse;

    type Error = BoxError;

    type Future = BoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx).map_err(Into::into)
    }

    fn call(&mut self, req: BinaryRequest) -> Self::Future {
        let opcode = req.opcode();
        self.inner
            .call(req)
            .map_err(Into::into)
            .and_then(move |resp| async move {
                if resp.opcode() != opcode {
                    return Err(io::Error::new(
                        io::ErrorKind::InvalidData,
                        format!("unexpected message opcode {:?}", resp),
                    )
                    .into());
                }
                Ok(resp)
            })
            .boxed()
    }
}
