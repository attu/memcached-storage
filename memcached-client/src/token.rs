use bytes::Bytes;
use serde::{de::DeserializeOwned, Serialize};
use tokio::io;

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Token {
    key: Bytes,
    cas: u64,
}

impl Token {
    pub(crate) fn make(key: Bytes, cas: u64) -> io::Result<Self> {
        if cas != 0 {
            Ok(Self { key, cas })
        } else {
            Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                "Zero CAS is not allowed",
            ))
        }
    }

    pub fn new<Key>(key: &Key, cas: u64) -> io::Result<Self>
    where
        Key: ?Sized + Serialize,
    {
        Self::make(Bytes::from(serde_json::to_vec(&key)?), cas)
    }

    pub fn force<Key>(key: &Key) -> io::Result<Self>
    where
        Key: ?Sized + Serialize,
    {
        Ok(Self {
            key: Bytes::from(serde_json::to_vec(&key)?),
            cas: 0,
        })
    }

    pub fn get_key<Key>(&self) -> crate::Result<Key>
    where
        Key: DeserializeOwned,
    {
        Ok(serde_json::from_slice(&self.key)?)
    }

    pub(crate) fn key(self) -> Bytes {
        self.key
    }

    pub(crate) fn cas(&self) -> u64 {
        self.cas
    }
}
