use crate::binary_message::BinaryMessageLayer;
use crate::match_opcode::MatchOpcodeLayer;
use crate::opaque::OpaqueLayer;
use crate::pool_manager::PoolManager;
use crate::raw_connection::RawConnection;
use crate::{BoxError, Request, Response, Result};
use deadpool::managed::Object;
use futures_util::future::BoxFuture;
use memcached_codec::ClientCodec;
use std::ops::DerefMut;
use std::task::{Context, Poll};
use std::{net::SocketAddr, sync::Arc};
use tokio::{net::TcpStream, sync::Mutex};
use tokio_util::codec::Framed;
use tower::util::BoxService;
use tower::{Service, ServiceBuilder};

pub(crate) struct Connection {
    service: BoxService<Request, Response, BoxError>,
    local_addr: SocketAddr,
    stream: Arc<Mutex<Framed<TcpStream, ClientCodec>>>,
}

impl Connection {
    pub async fn new(addr: SocketAddr) -> Result<Self> {
        let service = RawConnection::new(addr).await?;
        let local_addr = service.local_addr();
        let stream = service.stream().clone();
        let service = BoxService::new(
            ServiceBuilder::new()
                .layer(BinaryMessageLayer)
                .layer(MatchOpcodeLayer)
                .layer(OpaqueLayer::new(|| 0))
                .service(service),
        );
        Ok(Self {
            service,
            local_addr,
            stream,
        })
    }

    pub fn stream(&self) -> &Arc<Mutex<Framed<TcpStream, ClientCodec>>> {
        &self.stream
    }

    pub fn local_addr(&self) -> SocketAddr {
        self.local_addr
    }
}

impl Service<Request> for Connection {
    type Response = Response;
    type Error = BoxError;
    type Future = BoxFuture<'static, Result<Self::Response>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<()>> {
        self.service.poll_ready(cx)
    }

    fn call(&mut self, req: Request) -> Self::Future {
        self.service.call(req)
    }
}

impl Service<Request> for Object<PoolManager> {
    type Response = <Connection as Service<Request>>::Response;

    type Error = <Connection as Service<Request>>::Error;

    type Future = <Connection as Service<Request>>::Future;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<std::result::Result<(), Self::Error>> {
        self.deref_mut().poll_ready(cx)
    }

    fn call(&mut self, req: Request) -> Self::Future {
        self.deref_mut().call(req)
    }
}
