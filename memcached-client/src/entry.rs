use std::convert::TryFrom;

use crate::{BoxError, Result, Token};
use bytes::Bytes;
use serde::de::DeserializeOwned;

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Data(pub(crate) Bytes);

impl Data {
    pub fn get<T>(self) -> Result<T>
    where
        T: DeserializeOwned,
    {
        let value = serde_json::from_slice(&self.0)?;
        Ok(value)
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Entry {
    pub token: Token,
    pub value: Data,
}

pub struct Record<T> {
    pub token: Token,
    pub value: T,
}

impl<T> TryFrom<Entry> for Record<T>
where
    T: DeserializeOwned,
{
    type Error = BoxError;

    fn try_from(entry: Entry) -> Result<Self> {
        let value = serde_json::from_slice(&entry.value.0)?;
        Ok(Self {
            token: entry.token,
            value,
        })
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryInto;

    use super::*;
    #[test]
    fn can_convert_data() {
        let data = Data(Bytes::from_static(b"\"lorem\""));
        let value: String = data.get().unwrap();
        assert_eq!(value, "lorem");

        let entry = Entry {
            token: Token::force("lorem").unwrap(),
            value: Data(Bytes::from_static(b"\"ipsum\"")),
        };

        let record: Record<String> = entry.try_into().unwrap();
        assert_eq!(record.value, "ipsum");
    }
}
