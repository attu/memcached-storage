use crate::{BinaryRequest, BinaryResponse, Request, Response};
use futures_util::{future::BoxFuture, FutureExt, TryFutureExt};
use std::task::{Context, Poll};
use tower::{BoxError, Layer, Service};

pub(crate) struct BinaryMessageLayer;

impl<S> Layer<S> for BinaryMessageLayer
where
    S: Service<BinaryRequest, Response = BinaryResponse>,
    S::Error: Into<BoxError>,
{
    type Service = BinaryMessageService<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Self::Service { inner }
    }
}

pub(crate) struct BinaryMessageService<S> {
    inner: S,
}

impl<S> Service<Request> for BinaryMessageService<S>
where
    S: Service<BinaryRequest, Response = BinaryResponse>,
    S::Error: Into<BoxError> + 'static,
    S::Future: Sized + Send + 'static,
{
    type Response = Response;

    type Error = BoxError;

    type Future = BoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx).map_err(Into::into)
    }

    fn call(&mut self, req: Request) -> Self::Future {
        let key = req.0.key().unwrap_or_default();
        self.inner
            .call(req.0)
            .map_err(Into::into)
            .and_then(move |resp| async move { Response::new(key, resp) })
            .boxed()
    }
}
