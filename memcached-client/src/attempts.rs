use crate::{BoxError, Request, Response};
use futures_util::future::{ready, Ready};
use tower::retry::Policy;

#[derive(Debug, Clone)]
pub struct Attempts(pub usize);

impl Policy<Request, Response, BoxError> for Attempts {
    type Future = Ready<Self>;

    fn retry(&self, _: &Request, result: Result<&Response, &BoxError>) -> Option<Self::Future> {
        if self.0 == 0 || result.is_ok() {
            None
        } else {
            Some(ready(Self(self.0 - 1)))
        }
    }

    fn clone_request(&self, req: &Request) -> Option<Request> {
        Some(req.clone())
    }
}
