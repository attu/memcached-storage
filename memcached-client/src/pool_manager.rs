use crate::{BoxError, Connection, Result};
use async_trait::async_trait;
use deadpool::managed::{Manager, RecycleResult};
use std::net::SocketAddr;
use tokio::io::AsyncWriteExt;

pub(crate) struct PoolManager {
    pub addr: SocketAddr,
}

#[async_trait]
impl Manager for PoolManager {
    type Type = Connection;

    type Error = BoxError;

    async fn create(&self) -> Result<Connection> {
        Ok(Connection::new(self.addr).await?)
    }

    async fn recycle(&self, obj: &mut Connection) -> RecycleResult<BoxError> {
        const EMPTY: [u8; 0] = [];
        let stream = obj.stream().clone();
        let mut stream = stream.lock().await;
        stream
            .get_mut()
            .write(&EMPTY)
            .await
            .map_err(BoxError::from)
            .map_err(Into::into)
            .map(|_| ())
    }
}
