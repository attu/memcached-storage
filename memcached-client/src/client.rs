use crate::{
    attempts::Attempts, BoxError, ConnectionPool, Deletion, Entry, Expiry, Request, Response,
    Result, Token, Transaction,
};
use futures_util::future::BoxFuture;
use serde::Serialize;
use std::{
    net::SocketAddr,
    task::{Context, Poll},
    time::Duration,
};
use tokio::io;
use tower::{
    balance::p2c::Balance,
    load::{CompleteOnResponse, PendingRequestsDiscover},
    util::BoxService,
    Service, ServiceBuilder, ServiceExt,
};

pub struct Builder {
    timeout: Duration,
    retry: usize,
    connections: usize,
}

impl Default for Builder {
    fn default() -> Self {
        Self {
            timeout: Duration::from_secs(5),
            retry: 3,
            connections: 5,
        }
    }
}

impl Builder {
    pub fn timeout(mut self, timeout: Duration) -> Self {
        self.timeout = timeout;
        self
    }

    pub fn retry(mut self, retry: usize) -> Self {
        self.retry = retry;
        self
    }

    pub fn connections(mut self, connections: usize) -> Self {
        self.connections = connections;
        self
    }

    pub fn build(self, addr: SocketAddr) -> Client {
        let service = ConnectionPool::new(addr, self.connections);
        let service = Balance::new(PendingRequestsDiscover::new(
            service,
            CompleteOnResponse::default(),
        ));
        let service = BoxService::new(
            ServiceBuilder::new()
                .retry(Attempts(self.retry))
                .timeout(self.timeout)
                .buffer(self.connections)
                .service(service),
        );
        Client { service }
    }
}

pub struct Client {
    service: BoxService<Request, Response, BoxError>,
}

impl Service<Request> for Client {
    type Response = Response;
    type Error = BoxError;
    type Future = BoxFuture<'static, Result<Self::Response>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<()>> {
        self.service.poll_ready(cx)
    }

    fn call(&mut self, req: Request) -> Self::Future {
        self.service.call(req)
    }
}

impl Client {
    pub fn builder() -> Builder {
        Builder::default()
    }

    fn unexpected_message(resp: Response) -> BoxError {
        io::Error::new(
            io::ErrorKind::InvalidData,
            format!("unexpected message {:?}", resp),
        )
        .into()
    }

    pub async fn version(&mut self) -> Result<String> {
        self.ready()
            .await?
            .call(Request::version())
            .await
            .and_then(|resp| match resp {
                Response::Version(version) => Ok(version),
                _ => Err(Self::unexpected_message(resp)),
            })
    }

    pub async fn quit(mut self) -> Result<()> {
        self.ready()
            .await?
            .call(Request::quit())
            .await
            .and_then(|resp| match resp {
                Response::Quit => Ok(()),
                _ => Err(Self::unexpected_message(resp)),
            })
    }

    pub async fn add<Key, V>(&mut self, key: &Key, value: &V, expiry: Expiry) -> Result<Transaction>
    where
        Key: ?Sized + Serialize,
        V: ?Sized + Serialize,
    {
        self.ready()
            .await?
            .call(Request::add(key, value, expiry)?)
            .await
            .and_then(|resp| match resp {
                Response::Add(t) => Ok(t),
                _ => Err(Self::unexpected_message(resp)),
            })
    }

    pub async fn get<Key>(&mut self, key: &Key) -> Result<Option<Entry>>
    where
        Key: ?Sized + Serialize,
    {
        self.ready()
            .await?
            .call(Request::get(key)?)
            .await
            .and_then(|resp| match resp {
                Response::Get(entry) => Ok(entry),
                _ => Err(Self::unexpected_message(resp)),
            })
    }

    pub async fn replace<Value>(
        &mut self,
        token: Token,
        value: &Value,
        expiry: Expiry,
    ) -> Result<Option<Transaction>>
    where
        Value: ?Sized + Serialize,
    {
        self.ready()
            .await?
            .call(Request::replace(token, value, expiry)?)
            .await
            .and_then(|resp: Response| match resp {
                Response::Replace(t) => Ok(t),
                _ => Err(Self::unexpected_message(resp)),
            })
    }

    pub async fn set<Value>(
        &mut self,
        token: Token,
        value: &Value,
        expiry: Expiry,
    ) -> Result<Transaction>
    where
        Value: ?Sized + Serialize,
    {
        self.ready()
            .await?
            .call(Request::set(token, value, expiry)?)
            .await
            .and_then(|resp| match resp {
                Response::Set(t) => Ok(t),
                _ => Err(Self::unexpected_message(resp)),
            })
    }

    pub async fn delete(&mut self, token: Token) -> Result<Deletion> {
        self.ready()
            .await?
            .call(Request::delete(token))
            .await
            .and_then(|resp| match resp {
                Response::Delete(d) => Ok(d),
                _ => Err(Self::unexpected_message(resp)),
            })
    }

    pub async fn flush(&mut self, expiry: Expiry) -> Result<()> {
        self.ready()
            .await?
            .call(Request::flush(expiry))
            .await
            .and_then(|resp| match resp {
                Response::Flush => Ok(()),
                _ => Err(Self::unexpected_message(resp)),
            })
    }
}
