use crate::{
    transaction::TransactionError, BinaryResponse, Data, Deletion, Entry, Result, Token,
    Transaction,
};
use bytes::Bytes;
use memcached_codec::{response::Status, Opcode, StatusKind};
use std::io;

#[derive(Debug, PartialEq, Eq, Hash)]
pub enum Response {
    Get(Option<Entry>),
    Set(Transaction),
    Add(Transaction),
    Replace(Option<Transaction>),
    Delete(Deletion),
    Quit,
    Flush,
    Version(String),
}

impl Response {
    pub(crate) fn new(key: Bytes, resp: BinaryResponse) -> Result<Self> {
        let resp = match resp {
            BinaryResponse::Flush(_) => Response::Flush,
            BinaryResponse::Quit(_) => Response::Quit,
            BinaryResponse::Version(msg) => Response::Version(msg.0),
            BinaryResponse::Add(msg) => Response::Add(Transaction::Ok(Token::make(key, msg.cas)?)),
            BinaryResponse::Set(msg) => Response::Set(Transaction::Ok(Token::make(key, msg.cas)?)),
            BinaryResponse::Replace(msg) => {
                Response::Replace(Some(Transaction::Ok(Token::make(key, msg.cas)?)))
            }
            BinaryResponse::Delete(_) => Response::Delete(Ok(())),
            BinaryResponse::Get(msg) => Response::Get(Some(Entry {
                token: Token::make(key, msg.cas)?,
                value: Data(msg.value),
            })),
            BinaryResponse::Status(msg) => Self::from_status(msg)?,
        };
        Ok(resp)
    }

    fn from_status(status: Status) -> io::Result<Self> {
        match (&status.kind, &status.opcode) {
            (StatusKind::KeyNotFound, Opcode::GET) => Ok(Response::Get(None)),
            (StatusKind::KeyNotFound, Opcode::REPLACE) => Ok(Response::Replace(None)),
            (StatusKind::KeyNotFound, Opcode::DELETE) => Ok(Response::Delete(Ok(()))),
            (StatusKind::KeyExists, Opcode::SET) => {
                Ok(Response::Set(Err(TransactionError::Failed)))
            }
            (StatusKind::KeyExists, Opcode::ADD) => {
                Ok(Response::Add(Err(TransactionError::Failed)))
            }
            (StatusKind::KeyExists, Opcode::REPLACE) => {
                Ok(Response::Replace(Some(Err(TransactionError::Failed))))
            }
            (StatusKind::KeyExists, Opcode::DELETE) => {
                Ok(Response::Delete(Err(TransactionError::Failed)))
            }
            _ => Err(status.as_error()),
        }
    }
}
