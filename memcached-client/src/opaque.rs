use crate::{BinaryRequest, BinaryResponse};
use futures_util::{future::BoxFuture, FutureExt, TryFutureExt};
use memcached_codec::Opaque;
use std::{
    io,
    task::{Context, Poll},
};
use tower::{BoxError, Layer, Service};

pub(crate) struct OpaqueLayer<G> {
    gen: G,
}

impl<G> OpaqueLayer<G> {
    pub fn new(gen: G) -> Self {
        Self { gen }
    }
}

impl<S, G> Layer<S> for OpaqueLayer<G>
where
    S: Service<Opaque<BinaryRequest>, Response = Opaque<BinaryResponse>>,
    S::Error: Into<BoxError>,
    G: FnMut() -> u32 + Clone,
{
    type Service = OpaqueService<S, G>;

    fn layer(&self, inner: S) -> Self::Service {
        Self::Service {
            inner,
            gen: self.gen.clone(),
        }
    }
}

pub(crate) struct OpaqueService<S, G> {
    inner: S,
    gen: G,
}

impl<S, G> Service<BinaryRequest> for OpaqueService<S, G>
where
    S: Service<Opaque<BinaryRequest>, Response = Opaque<BinaryResponse>>,
    S::Error: Into<BoxError> + 'static,
    S::Future: Sized + Send + 'static,
    G: FnMut() -> u32 + Clone,
{
    type Response = BinaryResponse;

    type Error = BoxError;

    type Future = BoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx).map_err(Into::into)
    }

    fn call(&mut self, req: BinaryRequest) -> Self::Future {
        let opaque = (self.gen)();
        let req = req.with_opaque(opaque);
        self.inner
            .call(req)
            .map_err(Into::into)
            .and_then(move |resp| async move {
                if resp.opaque != opaque {
                    return Err(io::Error::new(
                        io::ErrorKind::InvalidData,
                        format!("unexpected message opaque {:?}", resp),
                    )
                    .into());
                }
                Ok(resp.payload)
            })
            .boxed()
    }
}
