use crate::{PoolManager, Result};
use deadpool::managed::{Object, Pool, PoolConfig, PoolError};
use futures_util::{future::BoxFuture, pin_mut, Future, FutureExt, Stream};
use std::{
    io,
    net::SocketAddr,
    pin::Pin,
    task::{Context, Poll},
};
use tower::{discover::Change, BoxError};

type FutureState =
    Option<BoxFuture<'static, std::result::Result<Object<PoolManager>, PoolError<BoxError>>>>;

pub(crate) struct ConnectionPool {
    inner: Pool<PoolManager>,
    state: FutureState,
}

impl ConnectionPool {
    pub fn new(addr: SocketAddr, max_size: usize) -> Self {
        let config = PoolConfig::new(max_size);
        let manager = PoolManager { addr };
        let inner = Pool::from_config(manager, config);
        Self {
            inner: inner.clone(),
            state: Some(async move { inner.get().await }.boxed()),
        }
    }
}

impl Stream for ConnectionPool {
    type Item = Result<Change<SocketAddr, Object<PoolManager>>>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let pool = self.inner.clone();
        let fut = self
            .state
            .get_or_insert_with(move || async move { pool.get().await }.boxed());
        pin_mut!(fut);
        match fut.poll(cx) {
            Poll::Ready(svc) => {
                self.state = None;
                let svc = svc
                    .map(|obj| Change::Insert(obj.local_addr(), obj))
                    .map_err(|err| match err {
                        PoolError::Backend(e) => e,
                        PoolError::Timeout(_) => {
                            BoxError::from(io::Error::new(io::ErrorKind::TimedOut, "pool timeout"))
                        }
                        PoolError::Closed => {
                            BoxError::from(io::Error::new(io::ErrorKind::BrokenPipe, "pool closed"))
                        }
                        PoolError::NoRuntimeSpecified => BoxError::from(io::Error::new(
                            io::ErrorKind::ConnectionRefused,
                            "no runtime specified",
                        )),
                    });
                Poll::Ready(Some(svc))
            }
            Poll::Pending => Poll::Pending,
        }
    }
}
