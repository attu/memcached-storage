use crate::{BinaryRequest, BinaryResponse};
use futures_util::{
    future::{BoxFuture, FutureExt},
    StreamExt,
};
use memcached_codec::{ClientCodec, Codec, Opaque};
use std::{
    net::SocketAddr,
    ops::DerefMut,
    sync::Arc,
    task::{Context, Poll},
};
use tokio::{net::TcpStream, sync::Mutex};
use tokio_util::codec::{Decoder, Framed};
use tower::{BoxError, Service};

#[derive(Debug, Clone)]
pub(crate) struct RawConnection {
    stream: Arc<Mutex<Framed<TcpStream, ClientCodec>>>,
    local_addr: SocketAddr,
}

impl RawConnection {
    pub async fn new(addr: SocketAddr) -> Result<Self, BoxError> {
        let stream = TcpStream::connect(addr).await?;
        let local_addr = stream.local_addr()?;
        Ok(Self {
            stream: Arc::new(Mutex::new(Codec::client().framed(stream))),
            local_addr,
        })
    }

    pub fn stream(&self) -> &Arc<Mutex<Framed<TcpStream, ClientCodec>>> {
        &self.stream
    }

    pub fn local_addr(&self) -> SocketAddr {
        self.local_addr
    }
}

impl Service<Opaque<BinaryRequest>> for RawConnection {
    type Response = Opaque<BinaryResponse>;

    type Error = BoxError;

    type Future = BoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, _: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Opaque<BinaryRequest>) -> Self::Future {
        use futures_util::sink::SinkExt;
        let con = self.clone();
        async move {
            let mut con = con.stream.lock().await;
            con.send(req).await?;
            con.deref_mut()
                .next()
                .await
                .unwrap_or_else(|| Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, "")))
                .map_err(Into::into)
        }
        .boxed()
    }
}
