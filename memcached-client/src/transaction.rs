use crate::Token;
use thiserror::Error;

#[derive(Error, Debug, PartialEq, Eq, Hash)]
pub enum TransactionError {
    #[error("transaction failed")]
    Failed,
}

pub type Transaction = std::result::Result<Token, TransactionError>;
pub type Deletion = std::result::Result<(), TransactionError>;
