use crate::{BinaryRequest, Expiry, Result, Token};
use bytes::Bytes;
use memcached_codec::request::{Add, Delete, Flush, Get, Quit, Replace, Set, Version};
use serde::Serialize;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct Request(pub(crate) BinaryRequest);

impl Request {
    pub fn get<Key>(key: &Key) -> Result<Self>
    where
        Key: ?Sized + Serialize,
    {
        let key = Bytes::from(serde_json::to_vec(&key)?);
        Ok(Self(BinaryRequest::from(Get { key })))
    }

    pub fn set<Value>(token: Token, value: &Value, expiry: Expiry) -> Result<Self>
    where
        Value: ?Sized + Serialize,
    {
        let value = Bytes::from(serde_json::to_vec(&value)?);
        let cas = token.cas();
        Ok(Self(BinaryRequest::from(Set {
            key: token.key(),
            cas,
            expiry,
            value,
        })))
    }

    pub fn replace<Value>(token: Token, value: &Value, expiry: Expiry) -> Result<Self>
    where
        Value: ?Sized + Serialize,
    {
        let value = Bytes::from(serde_json::to_vec(&value)?);
        let cas = token.cas();
        Ok(Self(BinaryRequest::from(Replace {
            key: token.key(),
            cas,
            expiry,
            value,
        })))
    }

    pub fn add<Key, V>(key: &Key, value: &V, expiry: Expiry) -> Result<Self>
    where
        Key: ?Sized + Serialize,
        V: ?Sized + Serialize,
    {
        let key = Bytes::from(serde_json::to_vec(&key)?);
        let value = Bytes::from(serde_json::to_vec(&value)?);
        Ok(Self(BinaryRequest::from(Add { key, value, expiry })))
    }

    pub fn version() -> Self {
        Self(BinaryRequest::from(Version))
    }

    pub fn quit() -> Self {
        Self(BinaryRequest::from(Quit))
    }

    pub fn flush(expiry: Expiry) -> Self {
        Self(BinaryRequest::from(Flush(expiry)))
    }

    pub fn delete(token: Token) -> Self {
        let cas = token.cas();
        Self(BinaryRequest::from(Delete {
            key: token.key(),
            cas,
        }))
    }
}
