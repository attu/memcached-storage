use futures::stream::TryStreamExt;
use memcached_storage::{LabelUpdate, Record, RecordId, Storage};
use memcached_test::Memcached;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashSet,
    path::{Path, PathBuf},
};
use testcontainers::clients::Cli;

#[derive(Serialize, Deserialize, Eq, Debug, PartialEq)]
pub struct File {
    pub name: String,
    pub group: Option<String>,
}

#[tokio::test]
async fn smoke_test() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut storage = Storage::builder().build(memcached.addr());

    let first_path = PathBuf::from("lorem/ipsum");
    let second_path = PathBuf::from("dolor/sit");

    let amet = File {
        name: "amet".into(),
        group: None,
    };

    let consectetur = Path::new("consectetur");
    let adipiscing = Path::new("adipiscing");
    let elit = Path::new("elit");

    let labels = [consectetur, adipiscing];
    storage
        .create(RecordId::new(&first_path, &labels), &amet)
        .await
        .unwrap();

    let labels = [consectetur];
    storage
        .create(RecordId::new(&second_path, &labels), &amet)
        .await
        .unwrap();

    let first: Record<File> = storage.read(&first_path).await.unwrap().unwrap();
    assert_eq!(first.value, amet);
    assert!(first.labels().contains(consectetur));
    assert!(first.labels().contains(adipiscing));

    let update = storage
        .update(
            first.token,
            &amet,
            LabelUpdate {
                to_add: &[elit],
                to_remove: &[adipiscing],
            },
        )
        .await
        .unwrap()
        .unwrap()
        .unwrap();

    let first: Record<File> = storage.read(&first_path).await.unwrap().unwrap();
    assert_eq!(update, first.token);

    let second: Record<File> = storage.read(&second_path).await.unwrap().unwrap();
    assert_eq!(second.value, amet);
    assert!(second.labels().contains(consectetur));
    assert!(!second.labels().contains(adipiscing));

    let recv: Vec<_> = storage
        .search::<File>(consectetur)
        .try_collect()
        .await
        .unwrap();

    assert!(recv.contains(&first));
    assert!(recv.contains(&second));

    let lookup = storage.lookup(consectetur).await.unwrap();
    assert!(lookup.contains(&first_path));
    assert!(lookup.contains(&second_path));

    let recv: Vec<_> = storage.search::<File>(elit).try_collect().await.unwrap();

    assert!(recv.contains(&first));
    assert!(!recv.contains(&second));

    storage.delete(first.token).await.unwrap();

    let recv: Vec<_> = storage
        .search::<File>(consectetur)
        .try_collect()
        .await
        .unwrap();
    assert_eq!(recv.len(), 1);
    assert!(recv.contains(&second));

    let lookup = storage.lookup(consectetur).await.unwrap();
    assert!(!lookup.contains(&first_path));
    assert!(lookup.contains(&second_path));

    storage.delete(second.token).await.unwrap();

    let recv: Vec<_> = storage
        .search::<File>(consectetur)
        .try_collect()
        .await
        .unwrap();
    assert!(recv.is_empty());

    let lookup = storage.lookup(consectetur).await.unwrap();
    assert!(!lookup.contains(&first_path));
    assert!(!lookup.contains(&second_path));
}

#[tokio::test]
async fn can_create_item_in_storage() {
    let docker = Cli::default();
    let memcached = Memcached::new(&docker).await;
    let mut storage = Storage::builder().build(memcached.addr());

    let path = Path::new("lorem/ipsum");
    let file = File {
        name: "dolor".into(),
        group: None,
    };

    let sit = Path::new("sit");
    let amet = Path::new("amet");
    let labels = [sit, amet];

    let token = storage
        .create(RecordId::new(path, &labels), &file)
        .await
        .unwrap();
    let labels: HashSet<_> = labels.iter().map(PathBuf::from).collect();
    assert_eq!(token.labels(), &labels);

    let record = storage.read::<File>(path).await.unwrap().unwrap();
    assert_eq!(record.value, file);
}
