pub(super) mod model;

use crate::{record::RecordToken, LabelUpdate, Record, RecordId, Search, Transaction};
use memcached_client::{BoxError, Client, Expiry, Result, Token};
use model::DataSchema;
use serde::{de::DeserializeOwned, Serialize};
use std::{
    collections::HashSet,
    convert::TryFrom,
    net::SocketAddr,
    path::{Path, PathBuf},
    sync::Arc,
    time::Duration,
    usize,
};
use tokio::sync::Mutex;

#[derive(Debug)]
pub struct Builder {
    prefix: PathBuf,
    ttl: Expiry,
    timeout: Duration,
    retry: usize,
    connections: usize,
}

impl Default for Builder {
    fn default() -> Self {
        Self {
            prefix: PathBuf::default(),
            ttl: Expiry::default(),
            timeout: Duration::from_secs(5),
            retry: 3,
            connections: 5,
        }
    }
}

impl Builder {
    pub fn prefix(mut self, path: &Path) -> Self {
        self.prefix = path.into();
        self
    }

    pub fn ttl(mut self, expiry: Expiry) -> Self {
        self.ttl = expiry;
        self
    }

    pub fn timeout(mut self, timeout: Duration) -> Self {
        self.timeout = timeout;
        self
    }

    pub fn retry(mut self, retry: usize) -> Self {
        self.retry = retry;
        self
    }

    pub fn connections(mut self, connections: usize) -> Self {
        self.connections = connections;
        self
    }

    pub fn build(self, addr: SocketAddr) -> Storage {
        Storage {
            client: Arc::new(Mutex::new(
                Client::builder()
                    .timeout(self.timeout)
                    .retry(self.retry)
                    .connections(self.connections)
                    .build(addr),
            )),
            prefix: self.prefix,
            ttl: self.ttl,
            retry: self.retry,
        }
    }
}

#[derive(Clone)]
pub struct Storage {
    client: Arc<Mutex<Client>>,
    prefix: PathBuf,
    ttl: Expiry,
    retry: usize,
}

fn key_for(prefix: &Path, path: &Path) -> PathBuf {
    Path::new("/data").join(prefix).join(path)
}

fn strip_prefix(key: PathBuf) -> PathBuf {
    key.iter().skip(2).collect()
}

fn ref_for(prefix: &Path, label: &Path) -> PathBuf {
    Path::new("/refs").join(prefix).join(label)
}

enum Insert {
    Append(Token),
    Add(PathBuf),
}

fn transaction_error() -> BoxError {
    tokio::io::Error::new(tokio::io::ErrorKind::AlreadyExists, "").into()
}

fn labels_after_update<'a>(
    existing: &'a HashSet<PathBuf>,
    update: &LabelUpdate<'a>,
) -> Vec<&'a Path> {
    let mut labels: Vec<_> = existing.iter().map(|p| p.as_path()).collect();
    labels.extend(update.to_add.iter());
    labels
        .into_iter()
        .filter(|p| !update.to_remove.contains(p))
        .collect()
}

impl Storage {
    pub fn builder() -> Builder {
        Builder::default()
    }

    async fn add_ref(&self, client: &mut Client, label: &Path, location: &Path) -> Result<()> {
        async fn update_ref(
            client: &mut Client,
            insert: Insert,
            location: &[PathBuf],
            expiry: Expiry,
        ) -> Result<Option<()>> {
            match insert {
                Insert::Append(token) => match client.replace(token, &location, expiry).await? {
                    Some(Ok(_)) => Ok(Some(())),
                    Some(Err(_)) | None => Ok(None),
                },
                Insert::Add(key) => match client.add(&key, &location, expiry).await? {
                    Ok(_) => Ok(Some(())),
                    Err(_) => Ok(None),
                },
            }
        }
        let key = ref_for(&self.prefix, label);
        match client.add(&key, &[location], self.ttl).await? {
            Ok(_) => Ok(()),
            Err(_) => {
                let location = location.to_owned();
                for _ in 0..self.retry {
                    let (insert, refs) = match self.lookup_ref(client, &key).await? {
                        Some((token, mut refs)) => {
                            if !refs.contains(&location) {
                                refs.push(location.clone());
                            }
                            (Insert::Append(token), refs)
                        }
                        None => (Insert::Add(key.clone()), vec![location.clone()]),
                    };
                    if update_ref(client, insert, &refs, self.ttl).await?.is_some() {
                        return Ok(());
                    }
                }
                Err(transaction_error())
            }
        }
    }

    async fn del_ref(&self, client: &mut Client, label: &Path, location: &Path) -> Result<()> {
        let key = ref_for(&self.prefix, label);
        for _ in 0..self.retry {
            match self.lookup_ref(client, &key).await? {
                Some((token, refs)) => {
                    let refs: Vec<_> = refs.into_iter().filter(|l| l != location).collect();
                    if refs.is_empty() {
                        if client.delete(token).await?.is_ok() {
                            return Ok(());
                        }
                    } else {
                        match client.replace(token, &refs, self.ttl).await? {
                            Some(Ok(_)) | None => return Ok(()),
                            Some(Err(_)) => {}
                        }
                    }
                }
                None => return Ok(()),
            }
        }
        Err(transaction_error())
    }

    pub async fn create<Value>(
        &mut self,
        record_id: RecordId<'_>,
        value: &Value,
    ) -> Result<RecordToken>
    where
        Value: ?Sized + Serialize,
    {
        let location = key_for(self.prefix.as_path(), record_id.path);
        let mut client = self.client.lock().await;
        let data = DataSchema::new(record_id.labels, value);
        let token = client.add(&location, &data, self.ttl).await??;
        let mut refs = HashSet::new();
        for label in data.labels.iter() {
            if self.add_ref(&mut client, label, &location).await.is_ok() {
                refs.insert(PathBuf::from(label));
            }
        }
        Ok(RecordToken {
            token,
            labels: refs,
        })
    }

    pub async fn fetch<Value>(&mut self, path: PathBuf) -> Result<Option<Record<Value>>>
    where
        Value: DeserializeOwned,
    {
        self.read(path.as_path()).await
    }

    pub async fn read<Value>(&mut self, path: &Path) -> Result<Option<Record<Value>>>
    where
        Value: DeserializeOwned,
    {
        let mut client = self.client.lock().await;
        Ok(client
            .get(&key_for(self.prefix.as_path(), path))
            .await?
            .map(Record::try_from)
            .transpose()?)
    }

    pub async fn update<Value>(
        &mut self,
        token: RecordToken,
        value: &Value,
        updates: LabelUpdate<'_>,
    ) -> Result<Option<Transaction>>
    where
        Value: ?Sized + Serialize,
    {
        let location = token.location()?;
        let labels = token.labels;
        let after_update = labels_after_update(&labels, &updates);
        let data = DataSchema::new(&after_update, value);
        let mut refs = HashSet::new();
        let mut client = self.client.lock().await;
        let token = match client.replace(token.token, &data, self.ttl).await? {
            Some(Ok(token)) => token,
            Some(Err(err)) => return Ok(Some(Err(err))),
            None => return Ok(None),
        };
        for label in labels.iter().filter(|p| !updates.contains(&p.as_path())) {
            if self
                .touch_ref(&mut client, &label)
                .await
                .ok()
                .flatten()
                .is_some()
            {
                refs.insert(label.clone());
            }
        }
        for label in updates.to_add.iter() {
            if self.add_ref(&mut client, label, &location).await.is_ok() {
                refs.insert(PathBuf::from(label));
            }
        }
        for label in updates.to_remove.iter() {
            self.del_ref(&mut client, &label, &location).await.ok();
        }
        Ok(Some(Ok(RecordToken {
            token,
            labels: refs,
        })))
    }

    pub async fn delete(&mut self, token: RecordToken) -> Result<()> {
        let mut client = self.client.lock().await;
        let location = token.location().ok();
        client.delete(token.token).await??;
        if let Some(location) = location {
            for label in token.labels.into_iter() {
                self.del_ref(&mut client, &label, &location).await.ok();
            }
        }
        Ok(())
    }

    async fn lookup_ref(
        &self,
        client: &mut Client,
        key: &Path,
    ) -> Result<Option<(Token, Vec<PathBuf>)>> {
        let location = if let Some(entry) = client.get(&key).await? {
            let record = memcached_client::Record::<Vec<PathBuf>>::try_from(entry)?;
            let refs = record
                .value
                .into_iter()
                .map(|path| key_for(&self.prefix, &path))
                .collect();
            Some((record.token, refs))
        } else {
            None
        };
        Ok(location)
    }

    async fn touch_ref(&self, client: &mut Client, label: &Path) -> Result<Option<()>> {
        let key = ref_for(&self.prefix, label);
        if let Some((token, paths)) = self.lookup_ref(client, &key).await? {
            if client.replace(token, &paths, self.ttl).await?.is_some() {
                Ok(Some(()))
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }

    pub(crate) async fn locations(&mut self, label: &Path) -> Result<Vec<PathBuf>> {
        let key = ref_for(&self.prefix, label);
        let mut client = self.client.lock().await;
        self.lookup_ref(&mut client, &key)
            .await
            .map(|x| x.map(|x| x.1).unwrap_or_default())
    }

    pub async fn lookup(&mut self, label: &Path) -> Result<Vec<PathBuf>> {
        Ok(self
            .locations(label)
            .await?
            .into_iter()
            .map(strip_prefix)
            .collect())
    }

    pub fn search<Value>(&mut self, label: &Path) -> Search<Value> {
        Search::new(self.clone(), label.to_owned())
    }
}
