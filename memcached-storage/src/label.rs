use std::path::Path;
const EMPTY: &[&Path] = &[];

pub struct LabelUpdate<'a> {
    pub to_add: &'a [&'a Path],
    pub to_remove: &'a [&'a Path],
}

impl Default for LabelUpdate<'_> {
    fn default() -> Self {
        Self {
            to_add: Self::empty(),
            to_remove: Self::empty(),
        }
    }
}

impl LabelUpdate<'_> {
    pub fn empty() -> &'static [&'static Path] {
        EMPTY
    }
    pub(crate) fn contains(&self, path: &Path) -> bool {
        self.to_add.contains(&path) || self.to_remove.contains(&path)
    }
}
