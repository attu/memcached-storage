use crate::DataSet;
use memcached_client::{BoxError, Entry, Result, Token, TransactionError};
use serde::de::DeserializeOwned;
use std::{
    collections::HashSet,
    convert::TryFrom,
    path::{Path, PathBuf},
};
pub type Transaction = std::result::Result<RecordToken, TransactionError>;

pub struct RecordId<'a> {
    pub(crate) path: &'a Path,
    pub(crate) labels: &'a [&'a Path],
}

impl<'a> RecordId<'a> {
    pub fn new(path: &'a Path, labels: &'a [&'a Path]) -> Self {
        Self { path, labels }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct RecordToken {
    pub(crate) token: Token,
    pub(crate) labels: HashSet<PathBuf>,
}

impl RecordToken {
    pub fn labels(&self) -> &HashSet<PathBuf> {
        &self.labels
    }
    pub(crate) fn location(&self) -> Result<PathBuf> {
        self.token.get_key()
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct Record<Value> {
    pub token: RecordToken,
    pub value: Value,
}

impl<Value> Record<Value> {
    pub fn labels(&self) -> &HashSet<PathBuf> {
        &self.token.labels
    }
}

impl<Value> TryFrom<Entry> for Record<Value>
where
    Value: DeserializeOwned,
{
    type Error = BoxError;

    fn try_from(entry: Entry) -> Result<Self> {
        let recv = memcached_client::Record::<DataSet<Value>>::try_from(entry)?;
        Ok(Self {
            token: RecordToken {
                token: recv.token,
                labels: recv.value.labels,
            },
            value: recv.value.value,
        })
    }
}
