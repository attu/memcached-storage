use crate::Record;
use futures::{
    future::{ready, BoxFuture},
    Stream,
};
use memcached_client::Result;
use serde::de::DeserializeOwned;
use std::{
    path::PathBuf,
    pin::Pin,
    task::{Context, Poll},
};

use crate::Storage;

pub struct Search<Value> {
    storage: Storage,
    init: Option<BoxFuture<'static, Result<Vec<PathBuf>>>>,
    items: Vec<PathBuf>,
    fut: Option<BoxFuture<'static, Result<Option<Record<Value>>>>>,
}

impl<Value> Search<Value> {
    pub(crate) fn new(storage: Storage, path: PathBuf) -> Self {
        let mut mem = storage.clone();
        let init = Box::pin(async move { mem.locations(&path).await });
        Self {
            storage,
            init: Some(init),
            items: vec![],
            fut: None,
        }
    }
}

impl<Value> Stream for Search<Value>
where
    Value: DeserializeOwned + Send + 'static,
{
    type Item = Result<Record<Value>>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        if let Some(init) = self.init.as_mut() {
            match init.as_mut().poll(cx) {
                Poll::Ready(items) => {
                    self.items = items?;
                    self.init = None;
                }
                Poll::Pending => {}
            }
            return Poll::Pending;
        }
        let mut storage = self.storage.clone();
        let mut fut = self.fut.take().unwrap_or_else(|| {
            let item = self.items.pop();
            if let Some(item) = item {
                Box::pin(async move { storage.fetch(item).await })
            } else {
                Box::pin(ready(Ok(None)))
            }
        });
        match fut.as_mut().poll(cx) {
            Poll::Ready(Ok(Some(record))) => Poll::Ready(Some(Ok(record))),
            Poll::Ready(Err(err)) => Poll::Ready(Some(Err(err))),
            Poll::Ready(Ok(None)) => {
                if self.items.is_empty() {
                    Poll::Ready(None)
                } else {
                    Poll::Pending
                }
            }
            Poll::Pending => {
                self.fut = Some(fut);
                Poll::Pending
            }
        }
    }
}
