use serde::{Deserialize, Serialize};
use std::{
    collections::HashSet,
    path::{Path, PathBuf},
};

#[derive(Serialize)]
pub(super) struct DataSchema<'a, Value: ?Sized> {
    pub labels: &'a [&'a Path],
    pub value: &'a Value,
}

impl<'a, Value: ?Sized> DataSchema<'a, Value> {
    pub fn new(labels: &'a [&'a Path], value: &'a Value) -> DataSchema<'a, Value> {
        DataSchema { labels, value }
    }
}

#[derive(Serialize, Deserialize)]
pub(crate) struct DataSet<Value> {
    pub labels: HashSet<PathBuf>,
    pub value: Value,
}
