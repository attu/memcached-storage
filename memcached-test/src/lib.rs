use memcached_client::Client;
use std::net::{IpAddr, SocketAddr};
use testcontainers::{
    clients::Cli,
    images::generic::{GenericImage, WaitFor},
    Container, Docker,
};

pub const MEMCACHED_VERSION: &str = "1.6.10";
const MEMCACHED_IMAGE: &str = "memcached";
const MEMCACHED_PORT: u16 = 11211;

pub struct Memcached<'a> {
    host: IpAddr,
    container: Container<'a, Cli, GenericImage>,
}

async fn docker_host() -> IpAddr {
    let docker_host = std::env::var("DOCKER_HOST")
        .ok()
        .and_then(|url| if url.is_empty() { None } else { Some(url) })
        .map(|url| {
            url.parse::<http::Uri>()
                .expect("failed to parse DOCKER_HOST")
        })
        .map(|url| url.host().expect("failed to parse DOCKER_HOST").to_string())
        .unwrap_or_else(|| "localhost".to_string());
    let docker_host = format!("{}:0", docker_host);
    tokio::net::lookup_host(docker_host)
        .await
        .expect("failed to lookup DOCKER_HOST")
        .next()
        .expect("failed to lookup DOCKER_HOST")
        .ip()
}

impl<'a> Memcached<'a> {
    pub async fn new(docker: &Cli) -> Memcached<'_> {
        let image = GenericImage::new(format!("{}:{}", MEMCACHED_IMAGE, MEMCACHED_VERSION))
            .with_wait_for(WaitFor::Nothing);
        let memcached = docker.run(image);
        let memcached = Memcached {
            host: docker_host().await,
            container: memcached,
        };
        memcached.wait_for_ready().await;
        memcached
    }

    pub fn addr(&self) -> SocketAddr {
        let port = loop {
            if let Some(port) = self.container.get_host_port(MEMCACHED_PORT) {
                break port;
            }
        };
        SocketAddr::new(self.host, port)
    }

    async fn wait_for_ready(&self) {
        loop {
            let mut client = Client::builder().retry(1).connections(1).build(self.addr());
            if client.version().await.is_ok() {
                return;
            }
        }
    }
}
