use std::time::Duration;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct Expiry {
    duration: Duration,
}

impl Default for Expiry {
    fn default() -> Self {
        Expiry {
            duration: Duration::from_secs(u64::MAX),
        }
    }
}

impl Expiry {
    pub fn from_secs(secs: u32) -> Self {
        Expiry {
            duration: Duration::from_secs(secs as u64),
        }
    }

    pub fn now() -> Self {
        Expiry::from_secs(0)
    }

    pub fn as_secs(&self) -> u32 {
        self.duration.as_secs() as u32
    }
}

impl From<Expiry> for bytes::Bytes {
    fn from(expiry: Expiry) -> Self {
        use bytes::BufMut;
        use bytes::BytesMut;

        let mut data = BytesMut::new();
        data.put_u32(expiry.as_secs());
        data.into()
    }
}
