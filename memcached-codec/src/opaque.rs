use crate::Message;
use std::convert::TryFrom;
use std::io;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct Opaque<Payload> {
    pub opaque: u32,
    pub payload: Payload,
}

impl<Payload> From<Opaque<Payload>> for Message
where
    Message: From<Payload>,
{
    fn from(src: Opaque<Payload>) -> Self {
        let mut msg: Message = src.payload.into();
        msg.opaque = src.opaque;
        msg
    }
}

impl<Payload> TryFrom<Message> for Opaque<Payload>
where
    Payload: TryFrom<Message, Error = io::Error>,
{
    type Error = io::Error;

    fn try_from(msg: Message) -> Result<Self, Self::Error> {
        Ok(Self {
            opaque: msg.opaque,
            payload: Payload::try_from(msg)?,
        })
    }
}
