pub struct BinaryCodec;
use super::Message;
use bytes::{Buf, BufMut};
use bytes::{Bytes, BytesMut};
use std::io;
use tokio_util::codec;

impl codec::Encoder<Message> for BinaryCodec {
    type Error = io::Error;

    fn encode(&mut self, item: Message, dst: &mut BytesMut) -> Result<(), Self::Error> {
        use std::convert::TryFrom;
        dst.put_u8(item.opcode);
        dst.put_u16(
            u16::try_from(item.key.len()).map_err(|_| {
                io::Error::new(io::ErrorKind::InvalidInput, "key length out of range")
            })?,
        );
        dst.put_u8(u8::try_from(item.extras.len()).map_err(|_| {
            io::Error::new(io::ErrorKind::InvalidInput, "extras length out of range")
        })?);
        dst.put_u8(0); // data type: raw bytes
        dst.put_u16(item.status);
        dst.put_u32(
            u32::try_from(item.key.len() + item.extras.len() + item.value.len()).map_err(|_| {
                io::Error::new(io::ErrorKind::InvalidInput, "body length out of range")
            })?,
        );
        dst.put_u32(item.opaque);
        dst.put_u64(item.cas);
        dst.put(item.extras.as_ref());
        dst.put(item.key.as_ref());
        dst.put(item.value.as_ref());
        Ok(())
    }
}

impl codec::Decoder for BinaryCodec {
    type Item = Message;
    type Error = io::Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        const HEADER_SIZE: usize = 23;
        let mut header = if let Some(header) = src.get(0..HEADER_SIZE).map(Bytes::copy_from_slice) {
            header
        } else {
            return Ok(None);
        };

        let opcode = header.get_u8();
        let key_length = header.get_u16() as usize;
        let extras_length = header.get_u8() as usize;
        let _data_type = header.get_u8();
        let status = header.get_u16();
        let total_body_length = header.get_u32() as usize;
        let opaque = header.get_u32();
        let cas = header.get_u64();
        if key_length + extras_length > total_body_length {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                "invalid total body length",
            ));
        }

        if (HEADER_SIZE + total_body_length) > src.remaining() {
            return Ok(None);
        }

        src.advance(HEADER_SIZE);
        let extras = src
            .get(0..extras_length)
            .map(Bytes::copy_from_slice)
            .unwrap_or_default();
        src.advance(extras_length);
        let key = src
            .get(0..key_length)
            .map(Bytes::copy_from_slice)
            .unwrap_or_default();
        src.advance(key_length);
        let value_length = total_body_length - key_length - extras_length;
        let value = src
            .get(0..value_length)
            .map(Bytes::copy_from_slice)
            .unwrap_or_default();
        src.advance(value_length);

        Ok(Message {
            opcode,
            status,
            opaque,
            cas,
            extras,
            key,
            value,
        }
        .into())
    }
}
