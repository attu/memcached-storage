use bytes::Bytes;
#[derive(Default, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Message {
    pub opcode: u8,
    pub status: u16,
    pub opaque: u32,
    pub cas: u64,
    pub extras: Bytes,
    pub key: Bytes,
    pub value: Bytes,
}

impl Message {
    pub fn with_opcode(mut self, opcode: u8) -> Self {
        self.opcode = opcode;
        self
    }
}
