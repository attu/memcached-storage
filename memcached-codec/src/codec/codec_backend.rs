use super::BinaryCodec;
use super::Message;
use bytes::BytesMut;
use bytes::{Buf, BufMut};
use std::io;
use tokio_util::codec;

mod magic {
    pub const REQUEST: u8 = 0x80;
    pub const RESPONSE: u8 = 0x81;
}

#[derive(Debug, Clone)]
pub struct CodecBackend<E, D> {
    pub(crate) encoder: E,
    pub(crate) decoder: D,
}

impl<E, D> CodecBackend<E, D> {
    pub fn new(encoder: E, decoder: D) -> Self {
        Self { encoder, decoder }
    }
}

#[cfg(test)]
impl<T> codec::Encoder<Message> for CodecBackend<T, T>
where
    T: codec::Encoder<Message, Error = io::Error>,
{
    type Error = io::Error;

    fn encode(&mut self, item: Message, dst: &mut BytesMut) -> Result<(), Self::Error> {
        self.encoder.encode(item, dst)
    }
}

#[cfg(test)]
impl<T> codec::Decoder for CodecBackend<T, T>
where
    T: codec::Decoder<Item = Message, Error = io::Error>,
{
    type Item = Message;
    type Error = io::Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        self.decoder.decode(src)
    }
}

#[derive(Debug, Clone)]
pub struct RequestCodec;

impl codec::Encoder<Message> for RequestCodec {
    type Error = io::Error;

    fn encode(&mut self, item: Message, dst: &mut BytesMut) -> Result<(), Self::Error> {
        dst.put_u8(magic::REQUEST);
        BinaryCodec.encode(item, dst)
    }
}

impl codec::Decoder for RequestCodec {
    type Item = Message;
    type Error = io::Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        if src.is_empty() {
            return Ok(None);
        }
        if src.get_u8() != magic::REQUEST {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                "expecting magic REQUEST",
            ));
        }
        BinaryCodec.decode(src)
    }
}

#[derive(Debug, Clone)]
pub struct ResponseCodec;

impl codec::Encoder<Message> for ResponseCodec {
    type Error = io::Error;

    fn encode(&mut self, item: Message, dst: &mut BytesMut) -> Result<(), Self::Error> {
        dst.put_u8(magic::RESPONSE);
        BinaryCodec.encode(item, dst)
    }
}

impl codec::Decoder for ResponseCodec {
    type Item = Message;
    type Error = io::Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        if src.is_empty() {
            return Ok(None);
        }
        if src.get_u8() != magic::RESPONSE {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                "expecting magic RESPONSE",
            ));
        }
        BinaryCodec.decode(src)
    }
}
