use bytes::Bytes;
use std::convert::TryFrom;
use std::io;

mod add;
mod delete;
mod flush;
mod get;
mod quit;
mod replace;
mod set;
mod status;
mod update;
mod version;

pub type Key = Bytes;
pub type Value = Bytes;
use crate::{Message, Opaque};
pub use status::response::StatusKind;

pub mod request {
    pub use super::add::request::Add;
    pub use super::delete::request::Delete;
    pub use super::flush::request::Flush;
    pub use super::get::request::Get;
    pub use super::quit::request::Quit;
    pub use super::replace::request::Replace;
    pub use super::set::request::Set;
    pub use super::version::request::Version;
}

pub mod response {
    pub use super::add::response::Add;
    pub use super::delete::response::Delete;
    pub use super::flush::response::Flush;
    pub use super::get::response::Get;
    pub use super::quit::response::Quit;
    pub use super::replace::response::Replace;
    pub use super::set::response::Set;
    pub use super::status::response::Status;
    pub use super::version::response::Version;
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Opcode {
    GET,
    SET,
    ADD,
    REPLACE,
    DELETE,
    QUIT,
    FLUSH,
    VERSION,
}

impl Opcode {
    pub(crate) fn as_value(&self) -> u8 {
        match self {
            Opcode::GET => 0x00,
            Opcode::SET => 0x01,
            Opcode::ADD => 0x02,
            Opcode::REPLACE => 0x03,
            Opcode::DELETE => 0x04,
            Opcode::QUIT => 0x07,
            Opcode::FLUSH => 0x08,
            Opcode::VERSION => 0x0b,
        }
    }
}

impl TryFrom<u8> for Opcode {
    type Error = io::Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x00 => Ok(Opcode::GET),
            0x01 => Ok(Opcode::SET),
            0x02 => Ok(Opcode::ADD),
            0x03 => Ok(Opcode::REPLACE),
            0x04 => Ok(Opcode::DELETE),
            0x07 => Ok(Opcode::QUIT),
            0x08 => Ok(Opcode::FLUSH),
            0x0b => Ok(Opcode::VERSION),
            _ => Err(io::Error::new(io::ErrorKind::InvalidData, "unknown opcode")),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Request {
    Add(request::Add),
    Delete(request::Delete),
    Flush(request::Flush),
    Get(request::Get),
    Quit(request::Quit),
    Replace(request::Replace),
    Set(request::Set),
    Version(request::Version),
}

impl Request {
    pub fn with_opaque(self, opaque: u32) -> Opaque<Self> {
        Opaque {
            opaque,
            payload: self,
        }
    }

    pub fn opcode(&self) -> Opcode {
        match &self {
            Request::Add(_) => Opcode::ADD,
            Request::Delete(_) => Opcode::DELETE,
            Request::Flush(_) => Opcode::FLUSH,
            Request::Get(_) => Opcode::GET,
            Request::Quit(_) => Opcode::QUIT,
            Request::Replace(_) => Opcode::REPLACE,
            Request::Set(_) => Opcode::SET,
            Request::Version(_) => Opcode::VERSION,
        }
    }

    pub fn key(&self) -> Option<Key> {
        match &self {
            Request::Add(msg) => msg.key.clone().into(),
            Request::Delete(msg) => msg.key.clone().into(),
            Request::Flush(_) => None,
            Request::Get(msg) => msg.key.clone().into(),
            Request::Quit(_) => None,
            Request::Replace(msg) => msg.key.clone().into(),
            Request::Set(msg) => msg.key.clone().into(),
            Request::Version(_) => None,
        }
    }
}

impl From<request::Add> for Request {
    fn from(req: request::Add) -> Self {
        Request::Add(req)
    }
}

impl From<request::Delete> for Request {
    fn from(req: request::Delete) -> Self {
        Request::Delete(req)
    }
}

impl From<request::Flush> for Request {
    fn from(req: request::Flush) -> Self {
        Request::Flush(req)
    }
}

impl From<request::Get> for Request {
    fn from(req: request::Get) -> Self {
        Request::Get(req)
    }
}

impl From<request::Quit> for Request {
    fn from(req: request::Quit) -> Self {
        Request::Quit(req)
    }
}

impl From<request::Replace> for Request {
    fn from(req: request::Replace) -> Self {
        Request::Replace(req)
    }
}

impl From<request::Set> for Request {
    fn from(req: request::Set) -> Self {
        Request::Set(req)
    }
}

impl From<request::Version> for Request {
    fn from(req: request::Version) -> Self {
        Request::Version(req)
    }
}

impl From<Request> for Message {
    fn from(req: Request) -> Self {
        match req {
            Request::Add(req) => Message::from(req),
            Request::Delete(req) => Message::from(req),
            Request::Flush(req) => Message::from(req),
            Request::Get(req) => Message::from(req),
            Request::Quit(req) => Message::from(req),
            Request::Replace(req) => Message::from(req),
            Request::Set(req) => Message::from(req),
            Request::Version(req) => Message::from(req),
        }
    }
}

impl TryFrom<Message> for Request {
    type Error = io::Error;

    fn try_from(msg: Message) -> Result<Self, Self::Error> {
        match Opcode::try_from(msg.opcode)? {
            Opcode::ADD => Ok(Request::Add(request::Add::try_from(msg)?)),
            Opcode::DELETE => Ok(Request::Delete(request::Delete::try_from(msg)?)),
            Opcode::FLUSH => Ok(Request::Flush(request::Flush::try_from(msg)?)),
            Opcode::GET => Ok(Request::Get(request::Get::try_from(msg)?)),
            Opcode::QUIT => Ok(Request::Quit(request::Quit::try_from(msg)?)),
            Opcode::REPLACE => Ok(Request::Replace(request::Replace::try_from(msg)?)),
            Opcode::SET => Ok(Request::Set(request::Set::try_from(msg)?)),
            Opcode::VERSION => Ok(Request::Version(request::Version::try_from(msg)?)),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Response {
    Add(response::Add),
    Delete(response::Delete),
    Flush(response::Flush),
    Get(response::Get),
    Quit(response::Quit),
    Replace(response::Replace),
    Set(response::Set),
    Version(response::Version),
    Status(response::Status),
}

impl Response {
    pub fn opcode(&self) -> Opcode {
        match &self {
            Response::Add(_) => Opcode::ADD,
            Response::Delete(_) => Opcode::DELETE,
            Response::Flush(_) => Opcode::FLUSH,
            Response::Get(_) => Opcode::GET,
            Response::Quit(_) => Opcode::QUIT,
            Response::Replace(_) => Opcode::REPLACE,
            Response::Set(_) => Opcode::SET,
            Response::Version(_) => Opcode::VERSION,
            Response::Status(s) => s.opcode.clone(),
        }
    }
}

impl From<Response> for Message {
    fn from(resp: Response) -> Self {
        match resp {
            Response::Add(resp) => Message::from(resp),
            Response::Delete(resp) => Message::from(resp),
            Response::Flush(resp) => Message::from(resp),
            Response::Get(resp) => Message::from(resp),
            Response::Quit(resp) => Message::from(resp),
            Response::Replace(resp) => Message::from(resp),
            Response::Set(resp) => Message::from(resp),
            Response::Version(resp) => Message::from(resp),
            Response::Status(status) => Message::from(status),
        }
    }
}

impl From<response::Add> for Response {
    fn from(resp: response::Add) -> Self {
        Response::Add(resp)
    }
}

impl From<response::Delete> for Response {
    fn from(resp: response::Delete) -> Self {
        Response::Delete(resp)
    }
}

impl From<response::Flush> for Response {
    fn from(resp: response::Flush) -> Self {
        Response::Flush(resp)
    }
}

impl From<response::Get> for Response {
    fn from(resp: response::Get) -> Self {
        Response::Get(resp)
    }
}

impl From<response::Quit> for Response {
    fn from(resp: response::Quit) -> Self {
        Response::Quit(resp)
    }
}

impl From<response::Replace> for Response {
    fn from(resp: response::Replace) -> Self {
        Response::Replace(resp)
    }
}

impl From<response::Set> for Response {
    fn from(resp: response::Set) -> Self {
        Response::Set(resp)
    }
}

impl From<response::Version> for Response {
    fn from(resp: response::Version) -> Self {
        Response::Version(resp)
    }
}

impl From<response::Status> for Response {
    fn from(status: response::Status) -> Self {
        Response::Status(status)
    }
}

impl TryFrom<Message> for Response {
    type Error = io::Error;

    fn try_from(msg: Message) -> Result<Self, Self::Error> {
        if StatusKind::try_from(msg.status)? == StatusKind::NoError {
            match Opcode::try_from(msg.opcode)? {
                Opcode::ADD => Ok(Response::from(response::Add::try_from(msg)?)),
                Opcode::DELETE => Ok(Response::from(response::Delete::try_from(msg)?)),
                Opcode::FLUSH => Ok(Response::from(response::Flush::try_from(msg)?)),
                Opcode::GET => Ok(Response::from(response::Get::try_from(msg)?)),
                Opcode::QUIT => Ok(Response::from(response::Quit::try_from(msg)?)),
                Opcode::REPLACE => Ok(Response::from(response::Replace::try_from(msg)?)),
                Opcode::SET => Ok(Response::from(response::Set::try_from(msg)?)),
                Opcode::VERSION => Ok(Response::from(response::Version::try_from(msg)?)),
            }
        } else {
            Ok(Response::from(response::Status::try_from(msg)?))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_convert_u8_to_opcode() {
        assert_eq!(
            Opcode::try_from(Opcode::GET.as_value()).unwrap(),
            Opcode::GET
        );
        assert_eq!(
            Opcode::try_from(Opcode::SET.as_value()).unwrap(),
            Opcode::SET
        );
        assert_eq!(
            Opcode::try_from(Opcode::ADD.as_value()).unwrap(),
            Opcode::ADD
        );
        assert_eq!(
            Opcode::try_from(Opcode::REPLACE.as_value()).unwrap(),
            Opcode::REPLACE
        );
        assert_eq!(
            Opcode::try_from(Opcode::DELETE.as_value()).unwrap(),
            Opcode::DELETE
        );
        assert_eq!(
            Opcode::try_from(Opcode::QUIT.as_value()).unwrap(),
            Opcode::QUIT
        );
        assert_eq!(
            Opcode::try_from(Opcode::FLUSH.as_value()).unwrap(),
            Opcode::FLUSH
        );
        assert_eq!(
            Opcode::try_from(Opcode::VERSION.as_value()).unwrap(),
            Opcode::VERSION
        );
        Opcode::try_from(255).unwrap_err();
    }
}
