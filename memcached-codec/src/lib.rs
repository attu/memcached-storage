//! memcached message codec
// #![deny(missing_docs)]

mod codec;
mod expiry;
mod messages;
mod opaque;

pub(crate) use codec::Message;
pub use codec::{ClientCodec, Codec, ServerCodec};
pub use expiry::Expiry;
pub use messages::{request, response, Key, Opcode, Request, Response, StatusKind, Value};
pub use opaque::Opaque;

#[cfg(test)]
mod tests {
    use super::*;
    use bytes::BytesMut;
    use std::convert::TryFrom;
    use std::io;
    use tokio_util::codec::{Decoder, Encoder};

    fn encode_decode<M, C>(req: &M, mut codec: C) -> M
    where
        M: Clone + Into<Message> + TryFrom<Message>,
        <M as TryFrom<Message>>::Error: std::fmt::Debug,
        C: Decoder<Item = Message, Error = io::Error> + Encoder<Message, Error = io::Error>,
    {
        let mut bytes = BytesMut::new();
        let req = req.clone().into();
        codec.encode(req, &mut bytes).unwrap();
        let resp = codec.decode(&mut bytes).unwrap().unwrap();
        M::try_from(resp).unwrap()
    }

    fn request_codec() -> codec::CodecBackend<codec::RequestCodec, codec::RequestCodec> {
        Codec::test_request_codec()
    }

    fn client_to_server(payload: &Request) -> Request {
        let mut client = Codec::client();
        let mut server = Codec::server();

        let mut bytes = BytesMut::new();
        let req = Opaque {
            payload: payload.clone(),
            opaque: 0x42,
        };
        client.encode(req, &mut bytes).unwrap();

        let decoded = server.decode(&mut bytes).unwrap().unwrap();
        assert_eq!(decoded.opaque, 0x42);
        decoded.payload
    }

    fn server_to_client(payload: &Response) -> Response {
        let mut client = Codec::client();
        let mut server = Codec::server();

        let mut bytes = BytesMut::new();
        let resp = Opaque {
            payload: payload.clone(),
            opaque: 0x42,
        };
        server.encode(resp, &mut bytes).unwrap();

        let decoded = client.decode(&mut bytes).unwrap().unwrap();
        assert_eq!(decoded.opaque, 0x42);
        decoded.payload
    }

    #[test]
    fn codec_can_encode_message() {
        let codec = request_codec();
        let req = Message::default();

        assert_eq!(req, encode_decode(&req, codec.clone()));

        let req = Message {
            opcode: 0x02,
            ..Default::default()
        };
        assert_eq!(req, encode_decode(&req, codec.clone()));

        let req = Message {
            status: 0x02,
            ..Default::default()
        };
        assert_eq!(req, encode_decode(&req, codec.clone()));

        let req = Message {
            opaque: 0x42,
            ..Default::default()
        };
        assert_eq!(req, encode_decode(&req, codec.clone()));

        let req = Message {
            cas: 0x42,
            ..Default::default()
        };
        assert_eq!(req, encode_decode(&req, codec.clone()));

        let req = Message {
            extras: "lorem".into(),
            ..Default::default()
        };
        assert_eq!(req, encode_decode(&req, codec.clone()));

        let req = Message {
            key: "ipsum".into(),
            ..Default::default()
        };
        assert_eq!(req, encode_decode(&req, codec.clone()));

        let req = Message {
            value: "dolor".into(),
            ..Default::default()
        };
        assert_eq!(req, encode_decode(&req, codec.clone()));

        let req = Message {
            opcode: 0x01,
            status: 0x02,
            opaque: 0x03,
            cas: 0x04,
            extras: "lorem".into(),
            key: "ipsum".into(),
            value: "dolor".into(),
        };
        assert_eq!(req, encode_decode(&req, codec));
    }

    #[test]
    fn client_and_server_codec_can_exchange_version_request() {
        let req = Request::from(request::Version);
        assert_eq!(req, client_to_server(&req));
    }

    #[test]
    fn client_and_server_codec_can_exchange_quit_request() {
        let req = Request::from(request::Quit);
        assert_eq!(req, client_to_server(&req));
    }

    #[test]
    fn client_and_server_codec_can_exchange_flush_request() {
        let req = Request::from(request::Flush(Expiry::from_secs(0x07)));
        assert_eq!(req, client_to_server(&req));
    }

    #[test]
    fn client_and_server_codec_can_exchange_get_request() {
        let req = Request::from(request::Get {
            key: Key::from_static(b"lorem"),
        });
        assert_eq!(req, client_to_server(&req));
    }

    #[test]
    fn client_and_server_codec_can_exchange_set_request() {
        let req = Request::from(request::Set {
            key: Key::from_static(b"lorem"),
            cas: 0x42,
            value: Value::from_static(b"ipsum"),
            expiry: Expiry::from_secs(0x13),
        });
        assert_eq!(req, client_to_server(&req));
    }

    #[test]
    fn client_and_server_codec_can_exchange_replace_request() {
        let req = Request::from(request::Replace {
            key: Key::from_static(b"lorem"),
            cas: 0x42,
            value: Value::from_static(b"ipsum"),
            expiry: Expiry::from_secs(0x13),
        });
        assert_eq!(req, client_to_server(&req));
    }

    #[test]
    fn client_and_server_codec_can_exchange_add_request() {
        let req = Request::from(request::Add {
            key: Key::from_static(b"lorem"),
            value: Value::from_static(b"ipsum"),
            expiry: Expiry::from_secs(0x13),
        });
        assert_eq!(req, client_to_server(&req));
    }

    #[test]
    fn client_and_server_codec_can_exchange_delete_request() {
        let req = Request::from(request::Delete {
            key: Key::from_static(b"lorem"),
            cas: 0x42,
        });
        assert_eq!(req, client_to_server(&req));
    }

    #[test]
    fn client_and_server_codec_can_exchange_version_response() {
        let resp = Response::from(response::Version("lorem".into()));
        assert_eq!(resp, server_to_client(&resp));
    }

    #[test]
    fn client_and_server_codec_can_exchange_quit_response() {
        let resp = Response::from(response::Quit);
        assert_eq!(resp, server_to_client(&resp));
    }

    #[test]
    fn client_and_server_codec_can_exchange_flush_response() {
        let resp = Response::from(response::Flush);
        assert_eq!(resp, server_to_client(&resp));
    }

    #[test]
    fn client_and_server_codec_can_exchange_get_response() {
        let resp = Response::from(response::Get {
            key: Key::from_static(b"lorem"),
            cas: 0x07,
            value: Value::from_static(b"ipsum"),
        });
        assert_eq!(resp, server_to_client(&resp));
    }

    #[test]
    fn client_and_server_codec_can_exchange_set_response() {
        let resp = Response::from(response::Set { cas: 0x07 });
        assert_eq!(resp, server_to_client(&resp));
    }

    #[test]
    fn client_and_server_codec_can_exchange_replace_response() {
        let resp = Response::from(response::Replace { cas: 0x07 });
        assert_eq!(resp, server_to_client(&resp));
    }

    #[test]
    fn client_and_server_codec_can_exchange_add_response() {
        let resp = Response::from(response::Add { cas: 0x07 });
        assert_eq!(resp, server_to_client(&resp));
    }

    #[test]
    fn client_and_server_codec_can_exchange_delete_response() {
        let resp = Response::from(response::Delete);
        assert_eq!(resp, server_to_client(&resp));
    }

    #[test]
    fn client_and_server_codec_can_exchange_status_response() {
        let resp = Response::from(response::Status {
            kind: StatusKind::AuthenticationError,
            opcode: Opcode::ADD,
            text: "lorem".into(),
        });
        assert_eq!(resp, server_to_client(&resp));
    }
}
