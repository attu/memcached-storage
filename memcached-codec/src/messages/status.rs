use crate::Message;
use crate::Opcode;
use std::convert::TryFrom;
use std::io;

pub mod response {
    use std::fmt::Display;

    use bytes::Bytes;

    use super::*;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub enum StatusKind {
        NoError,
        KeyNotFound,
        KeyExists,
        ValueTooLarge,
        InvalidArguments,
        ItemNotStored,
        IncrDecrOnNonNumericValue,
        TheVBucketBelongsToAnotherServer,
        AuthenticationError,
        AuthenticationContinue,
        UnknownCommand,
        OutOfMemory,
        NotSupported,
        InternalError,
        Busy,
        TemporaryFailure,
    }

    impl Display for StatusKind {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self)
        }
    }

    impl TryFrom<u16> for StatusKind {
        type Error = io::Error;

        fn try_from(status: u16) -> Result<Self, Self::Error> {
            match status {
                0x0000 => Ok(StatusKind::NoError),
                0x0001 => Ok(StatusKind::KeyNotFound),
                0x0002 => Ok(StatusKind::KeyExists),
                0x0003 => Ok(StatusKind::ValueTooLarge),
                0x0004 => Ok(StatusKind::InvalidArguments),
                0x0005 => Ok(StatusKind::ItemNotStored),
                0x0006 => Ok(StatusKind::IncrDecrOnNonNumericValue),
                0x0007 => Ok(StatusKind::TheVBucketBelongsToAnotherServer),
                0x0008 => Ok(StatusKind::AuthenticationError),
                0x0009 => Ok(StatusKind::AuthenticationContinue),
                0x0081 => Ok(StatusKind::UnknownCommand),
                0x0082 => Ok(StatusKind::OutOfMemory),
                0x0083 => Ok(StatusKind::NotSupported),
                0x0084 => Ok(StatusKind::InternalError),
                0x0085 => Ok(StatusKind::Busy),
                0x0086 => Ok(StatusKind::TemporaryFailure),
                _ => Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    "bad response status",
                )),
            }
        }
    }

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Status {
        pub kind: StatusKind,
        pub text: String,
        pub opcode: Opcode,
    }

    impl Display for Status {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self)
        }
    }

    impl Status {
        pub(crate) fn value(&self) -> u16 {
            match self.kind {
                StatusKind::NoError => 0x0000,
                StatusKind::KeyNotFound => 0x0001,
                StatusKind::KeyExists => 0x0002,
                StatusKind::ValueTooLarge => 0x0003,
                StatusKind::InvalidArguments => 0x0004,
                StatusKind::ItemNotStored => 0x0005,
                StatusKind::IncrDecrOnNonNumericValue => 0x0006,
                StatusKind::TheVBucketBelongsToAnotherServer => 0x0007,
                StatusKind::AuthenticationError => 0x0008,
                StatusKind::AuthenticationContinue => 0x0009,
                StatusKind::UnknownCommand => 0x0081,
                StatusKind::OutOfMemory => 0x0082,
                StatusKind::NotSupported => 0x0083,
                StatusKind::InternalError => 0x0084,
                StatusKind::Busy => 0x0085,
                StatusKind::TemporaryFailure => 0x0096,
            }
        }

        pub fn as_error(&self) -> io::Error {
            io::Error::new(io::ErrorKind::Other, format!("{}", self))
        }
    }

    impl TryFrom<Message> for Status {
        type Error = io::Error;

        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            let opcode = Opcode::try_from(msg.opcode)?;
            Ok(Self {
                kind: StatusKind::try_from(msg.status)?,
                text: std::str::from_utf8(msg.value.as_ref())
                    .map(ToOwned::to_owned)
                    .unwrap_or_default(),
                opcode,
            })
        }
    }

    impl From<Status> for Message {
        fn from(s: Status) -> Self {
            Self {
                opcode: s.opcode.as_value(),
                status: s.value(),
                value: Bytes::from(s.text),
                ..Default::default()
            }
        }
    }
}
