use super::Key;
use super::Value;
use crate::Expiry;
use crate::Message;
use std::convert::TryFrom;
use std::io;

pub mod request {
    use super::*;
    use bytes::{Buf, BufMut, BytesMut};

    #[derive(Default, Debug, PartialEq, Eq, Hash, Clone)]
    pub(crate) struct Update {
        pub key: Key,
        pub cas: u64,
        pub value: Value,
        pub expiry: Expiry,
    }

    impl TryFrom<Message> for Update {
        type Error = io::Error;

        fn try_from(mut msg: Message) -> Result<Self, Self::Error> {
            let expiry = if msg.extras.len() < 8 {
                Expiry::default()
            } else {
                msg.extras.get_u32(); // flags
                Expiry::from_secs(msg.extras.get_u32())
            };
            Ok(Self {
                key: msg.key,
                cas: msg.cas,
                value: msg.value,
                expiry,
            })
        }
    }

    impl From<Update> for Message {
        fn from(update: Update) -> Self {
            use bytes::Bytes;
            let mut extras = BytesMut::new();
            extras.put_u32(0); // flags
            extras.put::<Bytes>(update.expiry.into());
            Self {
                cas: update.cas,
                extras: extras.into(),
                key: update.key,
                value: update.value,
                ..Default::default()
            }
        }
    }
}

pub mod response {
    use super::*;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub(crate) struct Update {
        pub cas: u64,
    }

    impl TryFrom<Message> for Update {
        type Error = io::Error;

        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            if !msg.extras.is_empty() || !msg.key.is_empty() || !msg.value.is_empty() {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    "Update must not have extras, key nor value",
                ));
            }
            Ok(Self { cas: msg.cas })
        }
    }

    impl From<Update> for Message {
        fn from(set: Update) -> Self {
            Self {
                cas: set.cas,
                ..Default::default()
            }
        }
    }
}
