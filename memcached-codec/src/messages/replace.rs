use super::update;
use super::Key;
use super::Opcode;
use super::Value;
use crate::Expiry;
use crate::Message;
use std::convert::TryFrom;
use std::io;

pub mod request {
    use super::*;
    use update::request::Update;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Replace {
        pub key: Key,
        pub cas: u64,
        pub value: Value,
        pub expiry: Expiry,
    }

    impl TryFrom<Message> for Replace {
        type Error = io::Error;

        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            let update = Update::try_from(msg)?;

            Ok(Self {
                key: update.key,
                cas: update.cas,
                value: update.value,
                expiry: update.expiry,
            })
        }
    }

    impl From<Replace> for Message {
        fn from(replace: Replace) -> Self {
            Message::from(Update {
                key: replace.key,
                cas: replace.cas,
                value: replace.value,
                expiry: replace.expiry,
            })
            .with_opcode(Opcode::REPLACE.as_value())
        }
    }
}

pub mod response {
    use super::*;
    use update::response::Update;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Replace {
        pub cas: u64,
    }

    impl TryFrom<Message> for Replace {
        type Error = io::Error;

        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            Ok(Self {
                cas: Update::try_from(msg)?.cas,
            })
        }
    }

    impl From<Replace> for Message {
        fn from(replace: Replace) -> Self {
            Message::from(Update { cas: replace.cas }).with_opcode(Opcode::REPLACE.as_value())
        }
    }
}
