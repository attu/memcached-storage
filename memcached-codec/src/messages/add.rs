use super::update;
use super::Key;
use super::Opcode;
use super::Value;
use crate::Expiry;
use crate::Message;
use std::convert::TryFrom;
use std::io;

pub mod request {
    use super::*;
    use update::request::Update;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Add {
        pub key: Key,
        pub value: Value,
        pub expiry: Expiry,
    }

    impl TryFrom<Message> for Add {
        type Error = io::Error;

        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            let update = Update::try_from(msg)?;

            Ok(Self {
                key: update.key,
                value: update.value,
                expiry: update.expiry,
            })
        }
    }

    impl From<Add> for Message {
        fn from(add: Add) -> Self {
            Message::from(Update {
                key: add.key,
                value: add.value,
                expiry: add.expiry,
                ..Default::default()
            })
            .with_opcode(Opcode::ADD.as_value())
        }
    }
}

pub mod response {
    use super::*;
    use update::response::Update;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Add {
        pub cas: u64,
    }

    impl TryFrom<Message> for Add {
        type Error = io::Error;

        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            Ok(Self {
                cas: Update::try_from(msg)?.cas,
            })
        }
    }

    impl From<Add> for Message {
        fn from(add: Add) -> Self {
            Message::from(Update { cas: add.cas }).with_opcode(Opcode::ADD.as_value())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use bytes::Bytes;

    #[test]
    fn add_request_conversion_to_message() {
        let req = request::Add {
            key: Key::from_static(b"lorem"),
            value: Value::from_static(b"ipsum"),
            expiry: Expiry::default(),
        };
        let msg: Message = req.into();
        assert_eq!(msg.opcode, Opcode::ADD.as_value());
        assert_eq!(msg.status, 0);
        assert_eq!(msg.opaque, 0);
        assert_eq!(msg.cas, 0);
        assert_eq!(msg.key, Key::from_static(b"lorem"));
        assert_eq!(msg.value, Value::from_static(b"ipsum"));
        assert_eq!(msg.extras, Bytes::from_static(b"\0\0\0\0\xff\xff\xff\xff"));
    }

    #[test]
    fn add_response_conversion_to_message() {
        let resp = response::Add { cas: 42 };
        let msg: Message = resp.into();
        assert_eq!(msg.opcode, Opcode::ADD.as_value());
        assert_eq!(msg.status, 0);
        assert_eq!(msg.opaque, 0);
        assert_eq!(msg.cas, 42);
        assert!(msg.key.is_empty());
        assert!(msg.value.is_empty());
        assert!(msg.extras.is_empty());
    }
}
