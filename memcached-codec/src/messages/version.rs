use super::Opcode;
use crate::Message;
use std::convert::TryFrom;
use std::io;

pub mod request {
    use super::*;
    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Version;

    impl TryFrom<Message> for Version {
        type Error = io::Error;
        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            if !msg.extras.is_empty() || !msg.key.is_empty() || !msg.value.is_empty() {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    "Version must not have extras, key and value",
                ));
            }
            Ok(Version)
        }
    }

    impl From<Version> for Message {
        fn from(_: Version) -> Self {
            Self {
                opcode: Opcode::VERSION.as_value(),
                ..Default::default()
            }
        }
    }
}

pub mod response {
    use super::*;
    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Version(pub String);

    impl TryFrom<Message> for Version {
        type Error = io::Error;

        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            use std::str;
            if !msg.key.is_empty() || !msg.extras.is_empty() {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    "Version must not have key nor extras",
                ));
            }

            let version = str::from_utf8(msg.value.as_ref())
                .map(ToOwned::to_owned)
                .map_err(|_| {
                    io::Error::new(io::ErrorKind::InvalidData, "cannot decode version string")
                })?;
            Ok(Self(version))
        }
    }

    impl From<Version> for Message {
        fn from(v: Version) -> Self {
            Self {
                opcode: Opcode::VERSION.as_value(),
                value: v.0.into(),
                ..Default::default()
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{Key, Value};
    use bytes::Bytes;

    #[test]
    fn request_from_message_conversion() {
        request::Version::try_from(Message {
            key: Key::from_static(b"lorem"),
            ..Default::default()
        })
        .unwrap_err();

        request::Version::try_from(Message {
            value: Value::from_static(b"ipsum"),
            ..Default::default()
        })
        .unwrap_err();

        request::Version::try_from(Message {
            extras: Bytes::from_static(b"dolor"),
            ..Default::default()
        })
        .unwrap_err();
    }

    #[test]
    fn response_from_message_conversion() {
        response::Version::try_from(Message {
            key: Key::from_static(b"lorem"),
            ..Default::default()
        })
        .unwrap_err();

        let resp = response::Version::try_from(Message {
            value: Value::from_static(b"ipsum"),
            ..Default::default()
        })
        .unwrap();
        assert_eq!(resp.0, "ipsum");

        response::Version::try_from(Message {
            value: Value::from_static(b"\x81"),
            ..Default::default()
        })
        .unwrap_err();

        response::Version::try_from(Message {
            extras: Bytes::from_static(b"dolor"),
            ..Default::default()
        })
        .unwrap_err();
    }
}
