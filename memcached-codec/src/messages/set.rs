use super::update;
use super::Opcode;
use super::Value;
use crate::Expiry;
use crate::Message;
use std::convert::TryFrom;
use std::io;

pub mod request {
    use crate::Key;

    use super::*;
    use update::request::Update;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Set {
        pub key: Key,
        pub cas: u64,
        pub value: Value,
        pub expiry: Expiry,
    }

    impl TryFrom<Message> for Set {
        type Error = io::Error;

        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            let update = Update::try_from(msg)?;

            Ok(Self {
                key: update.key,
                cas: update.cas,
                value: update.value,
                expiry: update.expiry,
            })
        }
    }

    impl From<Set> for Message {
        fn from(set: Set) -> Self {
            Message::from(Update {
                key: set.key,
                cas: set.cas,
                value: set.value,
                expiry: set.expiry,
            })
            .with_opcode(Opcode::SET.as_value())
        }
    }
}

pub mod response {
    use super::*;
    use update::response::Update;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Set {
        pub cas: u64,
    }

    impl TryFrom<Message> for Set {
        type Error = io::Error;

        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            Ok(Self {
                cas: Update::try_from(msg)?.cas,
            })
        }
    }

    impl From<Set> for Message {
        fn from(set: Set) -> Self {
            Message::from(Update { cas: set.cas }).with_opcode(Opcode::SET.as_value())
        }
    }
}
