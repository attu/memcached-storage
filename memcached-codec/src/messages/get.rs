use super::Key;
use super::Opcode;
use super::Value;
use crate::Message;
use std::convert::TryFrom;
use std::io;

pub mod request {
    use super::*;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Get {
        pub key: Key,
    }

    impl TryFrom<Message> for Get {
        type Error = io::Error;
        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            if !msg.extras.is_empty() || !msg.value.is_empty() {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    "Get must not have extras nor value",
                ));
            }
            Ok(Get { key: msg.key })
        }
    }

    impl From<Get> for Message {
        fn from(get: Get) -> Self {
            Self {
                opcode: Opcode::GET.as_value(),
                key: get.key,
                ..Default::default()
            }
        }
    }
}

pub mod response {
    use super::*;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Get {
        pub key: Key,
        pub cas: u64,
        pub value: Value,
    }

    impl TryFrom<Message> for Get {
        type Error = io::Error;

        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            Ok(Self {
                key: msg.key,
                cas: msg.cas,
                value: msg.value,
            })
        }
    }

    impl From<Get> for Message {
        fn from(get: Get) -> Self {
            Self {
                opcode: Opcode::GET.as_value(),
                key: get.key,
                value: get.value,
                cas: get.cas,
                ..Default::default()
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use bytes::Bytes;

    #[test]
    fn request_from_message_conversion() {
        let req = request::Get::try_from(Message {
            key: Key::from_static(b"lorem"),
            ..Default::default()
        })
        .unwrap();
        assert_eq!(req.key, Key::from_static(b"lorem"));

        request::Get::try_from(Message {
            value: Value::from_static(b"ipsum"),
            ..Default::default()
        })
        .unwrap_err();

        request::Get::try_from(Message {
            extras: Bytes::from_static(b"dolor"),
            ..Default::default()
        })
        .unwrap_err();
    }
}
