use super::Opcode;
use crate::Message;
use std::convert::TryFrom;
use std::io;

pub mod request {
    use super::*;
    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Quit;

    impl TryFrom<Message> for Quit {
        type Error = io::Error;

        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            if !msg.extras.is_empty() || !msg.key.is_empty() || !msg.value.is_empty() {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    "Quit must not have extras, key and value",
                ));
            }
            Ok(Quit)
        }
    }

    impl From<Quit> for Message {
        fn from(_: Quit) -> Self {
            Self {
                opcode: Opcode::QUIT.as_value(),
                ..Default::default()
            }
        }
    }
}

pub mod response {
    use super::*;
    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Quit;

    impl TryFrom<Message> for Quit {
        type Error = io::Error;
        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            if !msg.extras.is_empty() || !msg.key.is_empty() || !msg.value.is_empty() {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    "Quit must not have extras, key nor value",
                ));
            }
            Ok(Self)
        }
    }

    impl From<Quit> for Message {
        fn from(_: Quit) -> Self {
            Self {
                opcode: Opcode::QUIT.as_value(),
                ..Default::default()
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{Key, Value};
    use bytes::Bytes;

    #[test]
    fn request_from_message_conversion() {
        request::Quit::try_from(Message {
            value: Key::from_static(b"lorem"),
            ..Default::default()
        })
        .unwrap_err();

        request::Quit::try_from(Message {
            value: Value::from_static(b"ipsum"),
            ..Default::default()
        })
        .unwrap_err();

        request::Quit::try_from(Message {
            extras: Bytes::from_static(b"dolor"),
            ..Default::default()
        })
        .unwrap_err();
    }

    #[test]
    fn response_from_message_conversion() {
        response::Quit::try_from(Message {
            value: Key::from_static(b"lorem"),
            ..Default::default()
        })
        .unwrap_err();

        response::Quit::try_from(Message {
            value: Value::from_static(b"ipsum"),
            ..Default::default()
        })
        .unwrap_err();

        response::Quit::try_from(Message {
            extras: Bytes::from_static(b"dolor"),
            ..Default::default()
        })
        .unwrap_err();
    }
}
