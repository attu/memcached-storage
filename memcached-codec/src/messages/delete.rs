use super::Key;
use super::Opcode;
use crate::Message;
use std::convert::TryFrom;
use std::io;

pub mod request {
    use super::*;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Delete {
        pub key: Key,
        pub cas: u64,
    }

    impl TryFrom<Message> for Delete {
        type Error = io::Error;
        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            if !msg.extras.is_empty() || !msg.value.is_empty() {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    "Delete must not have extras nor value",
                ));
            }
            Ok(Delete {
                key: msg.key,
                cas: msg.cas,
            })
        }
    }

    impl From<Delete> for Message {
        fn from(delete: Delete) -> Self {
            Self {
                opcode: Opcode::DELETE.as_value(),
                key: delete.key,
                cas: delete.cas,
                ..Default::default()
            }
        }
    }
}

pub mod response {
    use super::*;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Delete;

    impl TryFrom<Message> for Delete {
        type Error = io::Error;

        fn try_from(_: Message) -> Result<Self, Self::Error> {
            Ok(Self)
        }
    }

    impl From<Delete> for Message {
        fn from(_: Delete) -> Self {
            Self {
                opcode: Opcode::DELETE.as_value(),
                ..Default::default()
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Value;
    use bytes::Bytes;

    #[test]
    fn message_to_request_conversion() {
        let req = request::Delete::try_from(Message {
            key: Key::from_static(b"lorem"),
            cas: 42,
            ..Default::default()
        })
        .unwrap();
        assert_eq!(req.key, Key::from_static(b"lorem"));
        assert_eq!(req.cas, 42);

        request::Delete::try_from(Message {
            value: Value::from_static(b"ipsum"),
            ..Default::default()
        })
        .unwrap_err();

        request::Delete::try_from(Message {
            extras: Bytes::from_static(b"dolor"),
            ..Default::default()
        })
        .unwrap_err();
    }
}
