use super::Opcode;
use crate::{Expiry, Message};
use std::convert::TryFrom;
use std::io;

pub mod request {
    use super::*;
    use bytes::Buf;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Flush(pub Expiry);

    impl TryFrom<Message> for Flush {
        type Error = io::Error;

        fn try_from(mut msg: Message) -> Result<Self, Self::Error> {
            if !msg.key.is_empty() || !msg.value.is_empty() {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    "Flush must not have key and value",
                ));
            }
            let expiry = if msg.extras.len() < 4 {
                Expiry::default()
            } else {
                Expiry::from_secs(msg.extras.get_u32())
            };
            Ok(Self(expiry))
        }
    }

    impl From<Flush> for Message {
        fn from(flush: Flush) -> Self {
            Self {
                opcode: Opcode::FLUSH.as_value(),
                extras: flush.0.into(),
                ..Default::default()
            }
        }
    }
}

pub mod response {
    use super::*;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Flush;

    impl TryFrom<Message> for Flush {
        type Error = io::Error;
        fn try_from(msg: Message) -> Result<Self, Self::Error> {
            if !msg.extras.is_empty() || !msg.key.is_empty() || !msg.value.is_empty() {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    "Flush must not have extras, key nor value",
                ));
            }
            Ok(Self)
        }
    }

    impl From<Flush> for Message {
        fn from(_: Flush) -> Self {
            Self {
                opcode: Opcode::FLUSH.as_value(),
                ..Default::default()
            }
        }
    }
}
