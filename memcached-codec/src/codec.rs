pub mod binary_codec;
pub mod codec_backend;
pub mod message;

use crate::{Opaque, Request, Response};
use binary_codec::BinaryCodec;
pub use codec_backend::{CodecBackend, RequestCodec, ResponseCodec};
pub(crate) use message::Message;
use std::convert::TryFrom;
use std::io;
use tokio_util::codec;

pub struct Codec;

pub type ClientCodec = CodecBackend<RequestCodec, ResponseCodec>;
pub type ServerCodec = CodecBackend<ResponseCodec, RequestCodec>;

impl Codec {
    #[cfg(test)]
    pub fn test_request_codec() -> CodecBackend<RequestCodec, RequestCodec> {
        CodecBackend::new(RequestCodec, RequestCodec)
    }

    #[cfg(test)]
    pub fn test_response_codec() -> CodecBackend<ResponseCodec, ResponseCodec> {
        CodecBackend::new(ResponseCodec, ResponseCodec)
    }

    pub fn client() -> ClientCodec {
        CodecBackend::new(RequestCodec, ResponseCodec)
    }

    pub fn server() -> ServerCodec {
        CodecBackend::new(ResponseCodec, RequestCodec)
    }
}

impl codec::Encoder<Opaque<Request>> for ClientCodec {
    type Error = io::Error;

    fn encode(
        &mut self,
        item: Opaque<Request>,
        dst: &mut bytes::BytesMut,
    ) -> Result<(), Self::Error> {
        self.encoder.encode(Message::from(item), dst)
    }
}

impl codec::Decoder for ClientCodec {
    type Item = Opaque<Response>;
    type Error = io::Error;

    fn decode(&mut self, src: &mut bytes::BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        if let Some(msg) = self.decoder.decode(src)? {
            let opaque = msg.opaque;
            let payload = Response::try_from(msg)?;
            Ok(Some(Opaque { opaque, payload }))
        } else {
            Ok(None)
        }
    }
}

impl codec::Encoder<Opaque<Response>> for ServerCodec {
    type Error = io::Error;

    fn encode(
        &mut self,
        item: Opaque<Response>,
        dst: &mut bytes::BytesMut,
    ) -> Result<(), Self::Error> {
        self.encoder.encode(Message::from(item), dst)
    }
}

impl codec::Decoder for ServerCodec {
    type Item = Opaque<Request>;
    type Error = io::Error;

    fn decode(&mut self, src: &mut bytes::BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        if let Some(msg) = self.decoder.decode(src)? {
            let opaque = msg.opaque;
            let payload = Request::try_from(msg)?;
            Ok(Some(Opaque { opaque, payload }))
        } else {
            Ok(None)
        }
    }
}
